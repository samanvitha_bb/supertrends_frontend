import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Routes, RouterModule, Router, ActivatedRoute } from "@angular/router";

/**** Social Signin/Signup Services ***/
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { GoogleLoginProvider, FacebookLoginProvider, LinkedInLoginProvider } from 'angularx-social-login';

/**** Config File ***/
import { APIConfig } from '../apiconfig';

/**** Service File ***/
import { BaseService } from '../Services/base.service';
import { UtilityService } from '../utility.service';

@Component({
  selector: 'app-launch',
  templateUrl: './launch.component.html',
  styleUrls: ['./launch.component.scss']
})
export class LaunchComponent implements OnInit {

  userForm: FormGroup;
  userRegistrationForm: FormGroup;
  userLoginForm: FormGroup;

  user: SocialUser;
  isLogin: boolean = false;

  notifyMail: any = "";

  inviteRef: string = "";

  isNotify: boolean = true;
  showInvite: boolean = false;
  showLogin: boolean = false;
  showSignup: boolean = false;
  showDemoGraphics: boolean = false;

  step: any = 1;

  DemographicsInfo: any = { Gender: "" };

  data: Date;


  constructor(public _Config: APIConfig,
    public _baseService: BaseService,
    private _utilityService: UtilityService,
    private authService: AuthService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {

    this.route.queryParams
      .subscribe(params => {
        localStorage.setItem('refKey', params.ref);
      });


    this.initForms();

  }

  initForms() {
    this.userRegistrationForm = new FormGroup({
      FirstName: new FormControl('', Validators.required),
      LastName: new FormControl('', Validators.required),
      Email: new FormControl('', Validators.required),
      Password: new FormControl('', Validators.required),
      CPassword: new FormControl('', Validators.required),
    });

    this.userLoginForm = new FormGroup({
      Email: new FormControl('', Validators.required),
      Password: new FormControl('', Validators.required),
    });
  }



  ShowHideBy(type: any) {
    if (type == "Login") {
      this.showLogin = true;
      this.isNotify = false;
      this.showInvite = false;
      this.showSignup = false;
      this.showDemoGraphics = false;

    }
    else if (type == "SignUp") {
      this.showSignup = true;
      this.showLogin = false;
      this.isNotify = false;
      this.showInvite = false;
      this.showDemoGraphics = false;
    }
  }

  onFormSubmit() {
    let PostBody: any = {
      access_token: this._Config.MasterKey,
      email: this.notifyMail,
    };

    if (localStorage.getItem('refKey'))
      PostBody.reference_key = localStorage.getItem('refKey');

    let requestURL = this._Config.API_Root + this._Config.preLaunchUser;
    let headerOptions = this._baseService.commonHeaders("");
    this._baseService.doPOST(requestURL, PostBody, headerOptions)
      .subscribe(
        (response: any) => {

          let data: any = response.data;

          this.inviteRef = this._Config.Root_Domain_Share + "?ref=" + data.link_key;
          this.showInvite = true;
          this.isNotify = false;
        },
        (error: any) => {
          console.log("Rrror", error);
        }
      );


  }


  onUserRegistrationFormSubmit() {

    let PostBody = {
      firstName: this.userRegistrationForm.value.FirstName,
      lastName: this.userRegistrationForm.value.LastName,
      email: this.userRegistrationForm.value.Email,
      password: this.userRegistrationForm.value.Password,
      access_token: this._Config.MasterKey
    };


    let requestURL = this._Config.API_Root + this._Config.makeRegister;
    let headerOptions = this._baseService.commonHeaders("");
    this._baseService.doPOST(requestURL, PostBody, headerOptions)
      .subscribe(
        (result: any) => {

          if (result.statusCode == 200) {
            let data: any = result.data;

            data.user.isLogedIn = true;
            localStorage.setItem('userToken', data.token);
            localStorage.setItem('currentUser', JSON.stringify(data.user));
            this._baseService.currentUser = data.user;

            if (data.user.newProfile == true) {
              this.showDemoGraphics = true;
              this.showSignup = false;
              this.showLogin = false;
            }
            else {
              this._utilityService.showInfo('Welcome ' + data.user.firstName, 'You have loged in successfully.');
              //Redirect to Dashboard
              this.router.navigate(['dashboard']);
            }

          }
          else {
            // alert(result.message);
            // console.log("POST Request is successful ", result);
            this._utilityService.showError('Error!', result.message);
            return false;
          }


        },
        (error: any) => {
          console.log("Rrror", error);
        }
      );

  }



  onUserLoginFormSubmit() {

    var str = this.userLoginForm.value.Email + ":" + this.userLoginForm.value.Password;
    let base64Str = window.btoa(str);

    localStorage.setItem('userToken', base64Str);

    let PostBody = {
      access_token: this._Config.MasterKey
    };

    // this._baseService.makeLogin(PostBody);

    let requestURL = this._Config.API_Root + this._Config.makeLogin;
    let headerOptions = this._baseService.commonHeaders("withUserToken");
    this._baseService.doPOST(requestURL, PostBody, headerOptions)
      .subscribe(
        (result: any) => {
          debugger;

          if (result.statusCode == 200) {
            let data: any = result.data;

            data.user.isLogedIn = true;
            localStorage.setItem('userToken', data.token);
            localStorage.setItem('currentUser', JSON.stringify(data.user));
            this._baseService.currentUser = data.user;

            this._utilityService.showInfo('Welcome ' + data.user.firstName, 'You have loged in successfully.');
            //Redirect to Dashboard
            this.router.navigate(['dashboard']);

          }
          else {
            // alert(result.message);
            // console.log("POST Request is successful ", result);
            this._utilityService.showError('Error!', result.message);
            return false;
          }

        },
        (error: any) => {
          console.log("Rrror", error);
        }
      );


  }



  confirmSubmit() {
    var r = confirm("Are you sure you want to submit!");
    if (r == true) {
      this.updateDemographicsInfo();

    } else {
      this.showLogin = true;
      this.isNotify = false;
      this.showInvite = false;
      this.showSignup = false;
      this.showDemoGraphics = false;

    }
  }

  onValueChange(value: Date): void {
    this.data = value;
  }

  updateDemographicsInfo() {

    let PostBody: any = {
      email: this._baseService.currentUser.email,
      firstName: this._baseService.currentUser.firstName,
      newProfile: false,
    };

    //Set Optional Fields
    if (this.DemographicsInfo.DOB) {
      let timeStampFormatVal = Date.parse('2012-01-26T13:51:50.417-07:00');
      PostBody.dateOfBirth = Number(timeStampFormatVal);
    }

    if (this.DemographicsInfo.PlaceOfBirth)
      PostBody.birthPlace = this.DemographicsInfo.PlaceOfBirth;

    if (this.DemographicsInfo.City)
      PostBody.city = this.DemographicsInfo.City;

    if (this.DemographicsInfo.Education)
      PostBody.education = this.DemographicsInfo.Education;

    if (this.DemographicsInfo.Major)
      PostBody.major = this.DemographicsInfo.Major;

    if (this.DemographicsInfo.Occupation)
      PostBody.occupation = this.DemographicsInfo.Occupation;

    if (this.DemographicsInfo.Gender)
      PostBody.gender = this.DemographicsInfo.Gender;

    if (this.DemographicsInfo.Religion)
      PostBody.religion = this.DemographicsInfo.Religion;

    if (this.DemographicsInfo.Interests) {
      PostBody.interests = this.DemographicsInfo.Interests;
    }


    let requestURL = this._Config.API_Root + this._Config.UpdateProfile;
    let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
    this._baseService.doPUT(requestURL, PostBody, headerOptions)
      .subscribe(
        (result: any) => {
          if (result.statusCode == 200) {
            let data: any = result.data;

            this._baseService.getProfileInfo();
            this._utilityService.showInfo('Welcome ' + data.firstName, 'You have loged in successfully.');

            //Redirect to Dashboard
            this.router.navigate(['dashboard']);

          }
          else {
            this._utilityService.showError('Error!', result.message);
          }

        },
        (error: any) => {
          console.log("Rrror", error);
        }
      );


  }



  /********************* Social Signin/Signup *********************/

  authenticatedUser() {
    this.authService.authState.subscribe((user) => {
      this.user = user;
      debugger;

      if (this.user) {

        this.onUserSocialLogin();

      }


    });
  }


  signInWithGoogle(isLogin: boolean): void {
    this.isLogin = isLogin;
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    this.authenticatedUser();
  }

  signInWithFB(isLogin: boolean): void {
    this.isLogin = isLogin;
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
    this.authenticatedUser();
  }

  signOut(): void {
    this.authService.signOut();
  }


  onUserSocialLogin() {

    let PostBody = {
      access_token: this.user.authToken
    };


    let requestURL = "";
    if (this.user.provider == "FACEBOOK")
      requestURL = this._Config.API_Root + this._Config.makeFacebookLogin;
    else if (this.user.provider == "GOOGLE")
      requestURL = this._Config.API_Root + this._Config.makeGoogleLogin;

    // let requestURL = this._Config.API_Root + this._Config.makeFacebookLogin;
    let headerOptions = this._baseService.commonHeaders("");
    this._baseService.doPOST(requestURL, PostBody, headerOptions)
      .subscribe(
        (result: any) => {

          if (result.statusCode == 200) {
            let data: any = result.data;

            data.user.isLogedIn = true;
            localStorage.setItem('userToken', data.token);
            localStorage.setItem('currentUser', JSON.stringify(data.user));
            this._baseService.currentUser = data.user;

            if (data.user.newProfile) {
              this.showDemoGraphics = true;
              this.showSignup = false;
            }
            else {

              setTimeout(() => {
                this._utilityService.showInfo('Welcome ' + data.user.firstName, 'You have loged in successfully.');
                //Redirect to Dashboard
                this.router.navigate(['dashboard']);
              }, 1000);

            }


          }
          else {
            // alert(result.message);
            // console.log("POST Request is successful ", result);
            this._utilityService.showError('Error!', result.message);
            return false;
          }


        },
        (error: any) => {
          console.log("Rrror", error);
        }
      );


  }


}
