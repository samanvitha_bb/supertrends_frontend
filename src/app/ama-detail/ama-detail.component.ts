import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Routes, RouterModule, Router, ActivatedRoute } from "@angular/router";


/**** Config File ***/
import { APIConfig } from '../apiconfig';

import { BaseService } from '../Services/base.service';
import { PollService } from '../Services/poll.service';
import { UtilityService } from '../utility.service';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-ama-detail',
  templateUrl: './ama-detail.component.html',
  styleUrls: ['./ama-detail.component.scss']
})
export class AmaDetailComponent implements OnInit {
    @Input() selectedItem: any;
    @Output() isBack = new EventEmitter();

 constructor(public _Config: APIConfig, private _pollService: PollService, private _baseService: BaseService,
        private router: Router, private route: ActivatedRoute,  private _utilityService: UtilityService) {}
    ngOnInit() {
        if(this._baseService.currentUser.isLogedIn) {
            this.getSelectedAMAsDetails();
            this.getSelectedAMAsQuestion();
        }
        this.InitializeForm();
        this.selectedAMAsDetails = this.selectedItem;
        console.log('this.selectedAMAsDetails',  this.selectedItem);
    }
    id: any;
    
    discussionData: any;
    answerQuestion: any;
    isDummyData: boolean = true;
    selectedAMAsDetails: any;
    sortBYDropDownList = [{
        name: 'Newest',
        sortingKey: 'asc',
    }, {
         name: 'Oldest',
        sortingKey: 'desc',
    }];
    selectedCategory: string = "All";
    postQuestion: any = '';
    createQuestion: FormGroup;
    isOrderby:boolean= true;
    orderByDisplayLabel: string = 'Newest';

    getSelectedAMAsDetails() {
            this._baseService.isLoader = true;
            let requestURL = this._Config.API_Root + this._Config.getAllAMAList + '/' + this.selectedItem.id;
            console.log('getSelectedAMAsDetails-------', requestURL, this.selectedItem.id);
            let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
            this._baseService.doGET(requestURL, headerOptions)
              .subscribe(
                (response: any) => {
                    let result:any=response.data;
                    this.selectedAMAsDetails = result;
                    console.log('result----', result, response);
                },
                (error: any) => {
                    this._baseService.isLoader = false;
                    console.log("Rrror", error);
                }
              );
        }

        getSelectedAMAsQuestion() {
            let requestURL = this._Config.API_Root + this._Config.getAMAQuestions;
            requestURL = requestURL.replace("{AMA_ID}", this.selectedItem.id);
            let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
            this._baseService.doGET(requestURL, headerOptions)
              .subscribe(
                (response: any) => {
                    this._baseService.isLoader = false;
                    this.discussionData = response.data.rows;
                    this.discussionData.forEach(element => {
                        element.picture = "../../assets/img/profile.png";
                        element.firstName = "Travis";
                        element.lastName = "Steve";
                    });
                    console.log('this.discussionData-------------', this.discussionData);
                },
                (error: any) => {
                    this._baseService.isLoader = false;
                }
              );
        }

        selectOrderBy(sortingKey: any) {
            if(sortingKey == true) {
                this.orderByDisplayLabel = 'Newest';
            } else if(sortingKey == 'unAnswered') {
                this.orderByDisplayLabel = 'UnAnswered Question';
            } else {
                this.orderByDisplayLabel = 'Oldest';
            }
            this.isOrderby = sortingKey;
        }

        viewSelectedListDetails() {
            window.scroll(0, 0);  
        }

        //Initialize Form
        InitializeForm() {
            this.createQuestion = new FormGroup({
                questionTitile: new FormControl('', Validators.required),
                questionDescription: new FormControl('', Validators.required),
            });
        }

        goBack() {
            console.log('Calling FUnction0000000000000000000');
            this.isBack.emit(true);
        }

        createQuestionTOAMA() {
            let PostBody = {
                title: this.createQuestion.value.questionTitile,
                description: this.createQuestion.value.questionDescription,
            };
        
            this._baseService.isLoader = true;
        
            let requestURL = this._Config.API_Root + this._Config.addQuestionAMA;
            requestURL = requestURL.replace("{AMA_ID}", this.selectedItem.id);
    
            let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
            this._baseService.doPOST(requestURL, PostBody, headerOptions)
                .subscribe(
                  (response: any) => {
                    let responseData: any = response;
                    if (responseData.statusCode == 200) { 
                        this.getSelectedAMAsQuestion();
                        this.InitializeForm();
                        this.InitializeForm();
                        this._utilityService.showSuccess('Success!', "Question Asked Successfully.");
                        this.selectedAMAsDetails.comment_count = this.selectedAMAsDetails.comment_count + 1;
                    }
                    this._baseService.isLoader = false;
                  },
                  (error: any) => {
                    console.log("Rrror", error);
                  }
                );
        
            }


        likeAMA() {
            let requestURL = this._Config.API_Root + this._Config.likeAMA;
            requestURL = requestURL.replace("{AMA_ID}", this.selectedItem.id);
    
            let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
            this._baseService.doPUT(requestURL, {}, headerOptions)
            .subscribe(
                (response: any) => {
                let responseData: any = response;
                if (responseData.statusCode == 200) {
    
                    let responseObj = responseData.data;
                    if (responseObj) {
    
                    if (responseObj.like_count == this.selectedAMAsDetails.like_count) {
                        this._utilityService.showWarning('Warning!', "You have already liked this AMAs!");
                    }
                    else {
                        this.selectedAMAsDetails.like_count = responseObj.like_count;
                        this._utilityService.showSuccess('Success!', "AMAs Liked Successfully.");
                    }
                    }
                }
                else {
                    this._utilityService.showError('Error!', responseData.message);
                }
                },
                (error: any) => {
                console.log("Rrror", error);
                }
            );
        
        }

    likeQuestion(selectedQuestion: any, type: any) {

        let requestURL = this._Config.API_Root + this._Config.likeQuestionBYId;
        requestURL = requestURL.replace("{AMA_ID}", this.selectedAMAsDetails.id);
        requestURL = requestURL.replace("{QUES_ID}", selectedQuestion.id);
    
    
        let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
        this._baseService.doPUT(requestURL, {}, headerOptions)
            .subscribe(
            (response: any) => {
                let responseData: any = response;
                if (responseData.statusCode == 200) {
    
                let responseObj = responseData.data;
                if (responseObj) {
    
                    if (responseObj.like_count == selectedQuestion.like_count) {
                    this._utilityService.showWarning('Warning!', "You have already liked this Question!");
                    }
                    else {
    
                    if (type == 1) {
                        let itemIndex = this.discussionData.findIndex(obj => obj.id == selectedQuestion.id);
                        console.log('itemIndex-------------------------0', itemIndex);
                        this.discussionData[itemIndex].like_count = responseObj.like_count;
    
                    }
                    this._utilityService.showSuccess('Success!', "Question Liked Successfully.");
                    }
    
                }
    
                }
                else {
                this._utilityService.showError('Error!', responseData.message);
                }
            },
            (error: any) => {
                console.log("Rrror", error);
            }
            );
    }

    answerQuestionByAMA(selectedItem) {
        this.discussionData.forEach(element => {
            element.isReply = false;
        });
    
        let itemIndex = this.discussionData.findIndex(obj => obj.id == selectedItem.id);
        this.discussionData[itemIndex].isReply = true;
    }

    cancelAnswer() {
        this.discussionData.forEach(element => {
            element.isReply = false;
        });
        this.answerQuestion = "";
    }

    answerQuestions(selectedItem) {
        let PostBody = {
            answer: this.answerQuestion,
        };
        this._baseService.isLoader = true;
        let requestURL = this._Config.API_Root + this._Config.answerquestionAMA;
        requestURL = requestURL.replace("{AMA_ID}", this.selectedItem.id);
        requestURL = requestURL.replace("{QUES_ID}", selectedItem.id);

        let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
        this._baseService.doPUT(requestURL, PostBody, headerOptions)
        .subscribe(
            (response: any) => {

            let responseData: any = response;
            if (responseData.statusCode == 200) {

                this._baseService.isLoader = false;

                let responseObj = responseData.data;

                this.answerQuestion = "";
                this._utilityService.showSuccess('Success!', "Answered Posted Successfully.");

                this.selectedAMAsDetails.comment_count = this.selectedAMAsDetails.comment_count + 1;

            }
            else {
                this._utilityService.showError('Error!', responseData.message);
            }

            },
            (error: any) => {
            console.log("Rrror", error);
            }
        );
      }

}
