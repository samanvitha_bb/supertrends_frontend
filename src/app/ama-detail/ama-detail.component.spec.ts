import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmaDetailComponent } from './ama-detail.component';

describe('AmaDetailComponent', () => {
  let component: AmaDetailComponent;
  let fixture: ComponentFixture<AmaDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmaDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmaDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
