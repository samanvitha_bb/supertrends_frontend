import { Component, OnInit, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from "@angular/router";
import { FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { Observable, of } from 'rxjs';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { mergeMap } from 'rxjs/operators';


/**** Config File ***/
import { APIConfig } from '../apiconfig';

/**** Base Service File ***/
import { BaseService } from '../Services/base.service';
import { UtilityService } from '../utility.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  @ViewChild('showModel') showModel: ElementRef;
  @ViewChild('showInfoModel') showInfoModel: ElementRef;

  step: any = 1;

  DemographicsInfo: any = {
    Gender:"Male"
  };

  profileTab: any = 1;
  isEdit: boolean = false;

  trendingData = [
    {
      'title': 'When will Tesla make its entry into the Indian market?',
      'topic': 'trending',
      'category': 'automobile /  cars electrical cars',
      'user': 'Bob Business',
      'userimg': '',
      'time': '5  hours ago',
      'msgcount': '32',
      'count': '500',
      'cardbg': ''
    },
    {
      'title': 'When will Tesla make its entry into the Indian market3?',
      'topic': 'trending',
      'category': 'automobile /  cars electrical cars',
      'user': 'Bob Business',
      'userimg': '',
      'time': '',
      'msgcount': '32',
      'count': '567',
      'cardbg': ''
    },
    {
      'title': 'When will Tesla make its entry into the Indian market?',
      'topic': 'trending',
      'category': 'automobile /  cars electrical cars',
      'user': 'Bob Business',
      'userimg': '',
      'time': '',
      'msgcount': '32',
      'count': '213',
      'cardbg': ''
    }
  ];

  selectedFile: File;
  allFilesList: any = [];
  selectedCategory: any = "All";
  public uploader: FileUploader;
  public apiURL: any = "";

  modalRef: BsModalRef;
  message: string;

  data: Date;

  defaultImage = 'assets/img/touch.jpg';
  offset = 0;
  showDetails: boolean = false;
  selectedItem: any = {};


  configCountry: any = {
    displayKey: "country_name", //if objects array passed which key to be displayed defaults to description
    search: true,
    limitTo: 5,
    height: 'auto',
    placeholder: 'Select Country',
    noResultsFound: 'No results found!'
  };

  configState: any = {
    displayKey: "state_name", //if objects array passed which key to be displayed defaults to description
    search: true,
    limitTo: 5,
    height: 'auto',
    placeholder: 'Select State',
    noResultsFound: 'No results found!'
  };

  configCity: any = {
    displayKey: "city_name", //if objects array passed which key to be displayed defaults to description
    search: true,
    limitTo: 5,
    height: 'auto',
    placeholder: 'Select City',
    noResultsFound: 'No results found!'
  };


  


  


  countrySelect: any = [];
  countrySelected: any = {};
  StateSelected: any = {};
  CitySelected: any = {};

  constructor(public _Config: APIConfig, private _baseService: BaseService,
    private _utilityService: UtilityService,
    private modalService: BsModalService,
    private router: Router, private route: ActivatedRoute) {


    /********** File Upload Configurations **********/
    this.uploader = new FileUploader({ method: "PUT", url: this._Config.API_Root + this._Config.UpdateProfilePicture, itemAlias: 'picture', authToken: 'Bearer ' + localStorage.getItem('userToken') });
    this.apiURL = this._Config.API_Root + this._Config.UpdateProfilePicture;

    
  }

  ngOnInit() {



    if (this._baseService.currentUser.isLogedIn) {
      this._baseService.getProfileInfo();
      this._baseService.getMyPredictions();
      this._baseService.getAllPolls();
      this._baseService.getMyPolls();

      this._baseService.getCountries();


      /********** File Upload Configurations **********/
      this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
      this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {

        // console.log('ImageUpload:uploaded:', item, status, response);
        // alert('File uploaded successfully');
        let responseData: any = JSON.parse(response);
        if (responseData.statusCode == 200) {

          this._baseService.getProfileInfo();
          this._utilityService.showSuccess('Success!', 'Profile picture uploaded successfully.');

        }
        else {
          this._utilityService.showError('Error!', responseData.message);
          this._baseService.isLoader = false;
        }

      };

    }
    else {
      this.router.navigate(['timeline']);
    }


  }

 

  showHide(isEdit: boolean) {
    this.isEdit = isEdit;
    this.profileTab = 1;

  }

  /**
  * View Selected Poll Details
  * @param selectedItem 
  */
  ViewItem(selectedItem: any) {
    this.showDetails = true;
    this.selectedItem = selectedItem;
    window.scroll(0, 0);

  }


  /**
   * Poll Details page back button event
   * @param event 
   */
  handleBackChange(event: any) {
    this.showDetails = !event;
    this._baseService.selectMyPredictionCategory(this._baseService.selectedFilterPrediction);
  }

  /**
  * Redirect to Readmore page and set selected category
  * @param readType 
  */
  readMore(readType: any) {
    localStorage.setItem("readMore_Poll", readType);
    this.router.navigate(['polls/readmore']);

  }

  onValueChange(value: Date): void {
    this.data = value;
  }

  onFileSelected() {
    this.triggerConfirmModelPopup();
  }

  triggerConfirmModelPopup() {
    let el: HTMLElement = this.showModel.nativeElement as HTMLElement;
    el.click();
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  confirm(): void {
    this.uploader.uploadAll();
    this.message = 'Confirmed!';
    this.modalRef.hide();
  }

  decline(): void {
    this.message = 'Declined!';
    this.modalRef.hide();
  }


  updateDemographicsInfo() {

    let PostBody: any = {
      email: this._baseService.currentUser.email,
      firstName: this._baseService.currentUser.firstName,
    };

    //Set Optional Fields
    if (this.DemographicsInfo.FirstName)
      PostBody.firstName = this.DemographicsInfo.FirstName;

    if (this.DemographicsInfo.LastName)
      PostBody.lastName = this.DemographicsInfo.LastName;

    if (this.DemographicsInfo.DOB) {
      let timeStampFormatVal = Date.parse(this.DemographicsInfo.DOB);
      PostBody.dateOfBirth = Number(timeStampFormatVal);
    }

    if (this.DemographicsInfo.PlaceOfBirth)
      PostBody.birthPlace = this.DemographicsInfo.PlaceOfBirth;

    if (this.DemographicsInfo.City)
      PostBody.city = this.DemographicsInfo.City;

    if (this.DemographicsInfo.Country)
      PostBody.country = this.DemographicsInfo.Country;

    if (this.DemographicsInfo.Education)
      PostBody.education = this.DemographicsInfo.Education;

    if (this.DemographicsInfo.Major)
      PostBody.major = this.DemographicsInfo.Major;

    if (this.DemographicsInfo.Occupation)
      PostBody.occupation = this.DemographicsInfo.Occupation;

    if (this.DemographicsInfo.Gender)
      PostBody.gender = this.DemographicsInfo.Gender;

    if (this.DemographicsInfo.Religion)
      PostBody.religion = this.DemographicsInfo.Religion;

    if (this.DemographicsInfo.Interests) {
      PostBody.interests = this.DemographicsInfo.Interests;
    }


    let requestURL = this._Config.API_Root + this._Config.UpdateProfile;
    let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
    this._baseService.doPUT(requestURL, PostBody, headerOptions)
      .subscribe(
        (response: any) => {
          let responseData: any = response;
          if (responseData.statusCode == 200) {

            this._baseService.getProfileInfo();
            this._utilityService.showSuccess('Success!', 'Profile Info updated successfully.');
            this.isEdit = false;

            this.DemographicsInfo = {
              Gender:"Male"
            };

          }
          else {
            this._utilityService.showError('Error!', responseData.message);
          }

        },
        (error: any) => {
          console.log("Rrror", error);
        }
      );


  }



  confirmInfoUpdate(): void {
    this.updateDemographicsInfo();
    this.message = 'Confirmed!';
    this.modalRef.hide();
  }

  declineInfoUpdate(): void {
    this.message = 'Declined!';
    this.modalRef.hide();
  }


  onSelect(event: TypeaheadMatch): void {
    this.countrySelected = event.item;
  }




  changeCountryValue(selectedVal: any) {
    console.log(selectedVal);
    debugger;
    if (selectedVal.value) {
      if (selectedVal.value.length > 0) {
        this.countrySelected = selectedVal.value[0];
        this.DemographicsInfo.Country = this.countrySelected.country_name;
        this._baseService.getStatesByCountryID(this.countrySelected.id);
      }
    }


  }



  changeStateValue(selectedVal: any) {
    console.log(selectedVal);
    debugger;
    if (selectedVal.value) {
      if (selectedVal.value.length > 0) {
        this.StateSelected = selectedVal.value[0];
        this.DemographicsInfo.PlaceOfBirth = this.StateSelected.state_name;
        this._baseService.getCitiesByStateID(this.StateSelected.id);

      }
    }

  }



  changeCityValue(selectedVal: any) {
    console.log(selectedVal);
    debugger;
    if (selectedVal.value) {
      if (selectedVal.value.length > 0) {
        this.CitySelected = selectedVal.value[0];
        this.DemographicsInfo.PlaceOfBirth = this.CitySelected.city_name;
      }
    }

  }




  typeaheadOnCountrySelect(e: TypeaheadMatch): void {
    console.log('Selected value: ', e.item);
    this._baseService.getStatesByCountryID(e.item.id);
  }

  typeaheadOnStateSelect(e: TypeaheadMatch): void {
    console.log('Selected value: ', e.item);
    this._baseService.getCitiesByStateID(e.item.id);

  }

  typeaheadOnCitySelect(e: TypeaheadMatch): void {
    console.log('Selected value: ', e.item);
  }

}
