import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Routes, RouterModule, Router, ActivatedRoute } from "@angular/router";


/**** Config File ***/
import { APIConfig } from '../../apiconfig';

/**** Base Service File ***/
import { BaseService } from '../../Services/base.service';
import { UtilityService } from '../../utility.service';

@Component({
  selector: 'app-poll-details',
  templateUrl: './poll-details.component.html',
  styleUrls: ['./poll-details.component.scss']
})
export class PollDetailsComponent implements OnInit {

  @Input() selectedItem: any;
  @Input() type: any;
  @Output() isBack = new EventEmitter();

  selectedItemType: any = "";
  selectedItemInfo: any = {};
  postCommentInput: any = "";
  isOrderby: boolean = true;


  defaultImage = 'assets/img/touch.jpg';
  offset = 0;

  constructor(public _Config: APIConfig,
    private _baseService: BaseService,
    private _utilityService: UtilityService,
    private router: Router, private route: ActivatedRoute) {


  }


  ngOnInit() {

    if (this.type == "Poll")
      this.selectedItemType = "Poll";

    this.selectedItemInfo = this.selectedItem;

    if (this._baseService.currentUser.isLogedIn) {
      this.ViewDetailItem();
      this._baseService.getPollsCommentsByID(this.selectedItem.id);
    }

  }



  /**
   * Poll Details page back button event and Emit the Boolean Value True
   * @param event 
   */
  goBack() {
    this.isBack.emit(true);
  }


  /**
   * To make Order by Ascending or Descending
   * @param type 
   */
  selectOrderBy(type: boolean) {
    this.isOrderby = type;
  }

  ViewDetailItem() {
    if (this._baseService.currentUser.isLogedIn) {

      let requestURL = this._Config.API_Root + this._Config.getPollByID;
      requestURL = requestURL.replace("{PollID}", this.selectedItem.id);

      let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
      this._baseService.doGET(requestURL, headerOptions)
        .subscribe(
          (response: any) => {

          },
          (error: any) => {
            console.log("Rrror", error);
          }
        );

    }
  }

  likeItem() {
    if (this._baseService.currentUser.isLogedIn) {

      let requestURL = this._Config.API_Root + this._Config.LikePollById;
      requestURL = requestURL.replace("{PollID}", this.selectedItem.id);

      let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
      this._baseService.doPUT(requestURL, {}, headerOptions)
        .subscribe(
          (response: any) => {
            let responseData: any = response;
            if (responseData.statusCode == 200) {

              let responseObj = responseData.data;
              if (responseObj) {

                if (responseObj.like_count == this.selectedItem.like_count) {
                  this._utilityService.showWarning('Warning!', "You have already liked this poll!");
                }
                else {
                  this.selectedItem.like_count = responseObj.like_count;
                  this._utilityService.showSuccess('Success!', "Poll Liked Successfully.");
                }

              }



            }
            else {
              this._utilityService.showError('Error!', responseData.message);
            }
          },
          (error: any) => {
            console.log("Rrror", error);
          }
        );

    }
  }

  
  postComment() {

    if (this._baseService.currentUser.isLogedIn) {

      let PostBody = {
        comment: this.postCommentInput,
      };

      this._baseService.isLoader = true;

      let requestURL = this._Config.API_Root + this._Config.UpdatePollCommentById;
      requestURL = requestURL.replace("{PollID}", this.selectedItem.id);

      let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
      this._baseService.doPUT(requestURL, PostBody, headerOptions)
        .subscribe(
          (response: any) => {

            let responseData: any = response;
            if (responseData.statusCode == 200) {

              this._baseService.isLoader = false;


              let responseObj = responseData.data;
              if (responseObj)
                this._baseService.getPollsCommentsByID(responseObj.id);

              this.postCommentInput = "";
              this._utilityService.showSuccess('Success!', "Comment Posted Successfully.");

              this.selectedItemInfo.comment_count=this.selectedItemInfo.comment_count+1;


            }
            else {
              this._utilityService.showError('Error!', responseData.message);
            }

          },
          (error: any) => {
            console.log("Rrror", error);
          }
        );

    }
    else {
      this._utilityService.showWarning("", "Please Login and post your comment!");
    }


  }

}

