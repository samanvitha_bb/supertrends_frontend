import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
 

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  constructor(private toastr: ToastrService) { }

  showSuccess(title:any,msg:any) {
    // this.toastr.success('You are awesome!', 'Success!');
    this.toastr.success(msg,title);
  }

  showError(title:any,msg:any) {
    // this.toastr.error('This is not good!', 'Oops!');
    this.toastr.error(msg,title);
  }

  showWarning(title:any,msg:any) {
    // this.toastr.warning('You are being warned.', 'Alert!');
    this.toastr.warning(msg,title);
  }

  showInfo(title:any,msg:any) {
    // this.toastr.info('Just some information for you.');
    this.toastr.info(msg,title);
  }
  


  
  

}
