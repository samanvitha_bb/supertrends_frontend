import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from "@angular/router";

/**** Config File ***/
import { APIConfig } from '../../apiconfig';

/**** Base Service File ***/
import { BaseService } from '../../Services/base.service';
import { UtilityService } from '../../utility.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {

  viewType: any = "Users";

  constructor(public _Config: APIConfig, public _baseService: BaseService,
    private _utilityService: UtilityService,
    private router: Router, private route: ActivatedRoute) { }


  ngOnInit() {
    this._baseService.getAllCategories();
    console.log('_baseService.allLatestCategories', this._baseService.allLatestCategories);

  }


  ViewChange(Type: any) {
    this.viewType = Type;
  }
}
