import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'; 

/**** Config File ***/
import { APIConfig } from 'src/app/apiconfig';

import { BaseService } from 'src/app/Services/base.service';
import { UtilityService } from 'src/app/utility.service';


import { async } from '@angular/core/testing';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'] 
})
export class CategoryComponent implements OnInit {
  @ViewChild('modalDismiss') modalDismiss: ElementRef;
  @ViewChild('defaultTab') defaultTab: ElementRef;
  selectedCategory: any;
  createCategoryForm: FormGroup;
  formsHeading: any;
  isCategoryEdit: boolean = false;
  isCategoryAdd: boolean = false;
  formButtonsDisplayName = "Create Category";

  constructor(public _Config: APIConfig, private _baseService: BaseService, private _utilityService: UtilityService) {}

    ngOnInit() {
        this.InitializeForm();
        this._baseService.getAllCategories();
    }

    removeCategory() {
        this._baseService.isLoader = true;
        let requestURL = this._Config.API_Root + this._Config.DeleteCategory;
        requestURL = requestURL.replace("{catID}", this.selectedCategory.id);
        let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
        console.log('requestURL---------create Question', requestURL, headerOptions);
        this._baseService.doDelete(requestURL, headerOptions)
        .subscribe(
            (response: any) => {
                let responseData: any = response;
                if (responseData.statusCode == 200) {
                    this.dismissModalAfterAction();
                    this._baseService.isLoader = false;
                    this._baseService.getAllCategories();
                    this._utilityService.showSuccess("Success!", "Category is removed successfully.");
                }
                else {
                    this._baseService.isLoader = false;
                    this._utilityService.showError('Error!', responseData.message);
                }
            },
            (error: any) => {
            console.log("Rrror", error);
            }
        );
    }

   /**
   * Close Model after Actions Completed
   */
    dismissModalAfterAction() {
        let el: HTMLElement = this.modalDismiss.nativeElement as HTMLElement;
        el.click();
    }

     /**
     * OPen Default Tab in Category Dashboard
     */
    // selectDefaultTab(event) {
    //     console.log('event-------------',event);
    //     event.preventDefault();
    //     console.log(this.defaultTab.nativeElement as HTMLElement);
    //     let defaultTab: HTMLElement = this.defaultTab.nativeElement as HTMLElement;
    //     console.log('el', defaultTab);
    //     defaultTab.click();
    //     console.log('Selcet Default Tab Selection');
    // }

    selectCategory(category?: any, typeOfAction?: any) {
        this.selectedCategory = category;
        if(typeOfAction == 'isCategoryEdit') {
            this.isCategoryEdit = true;
            this.formButtonsLabel();
            this.updateCategoryPanel();
        } else if( typeOfAction == 'isCategoryAdd'){
            this.formButtonsLabel();
            this.isCategoryAdd = true;
            this.isCategoryEdit = true;
        }
        console.log(this.isCategoryEdit, typeOfAction);
    }

    //Initialize Form
	InitializeForm() {
        this.createCategoryForm = new FormGroup({
            categoryName: new FormControl('', Validators.required),
            description: new FormControl('', Validators.required),
        });
	}
	
	categoryAction() {
        if(this.isCategoryEdit && this.isCategoryAdd) {
            this._baseService.isLoader = true;
            let requestURL = this._Config.API_Root + this._Config.CreateCategory;

            let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
            this._baseService.doPOST(requestURL, {name:  this.createCategoryForm.value.categoryName, description: this.createCategoryForm.value.description}, headerOptions)
            .subscribe(
            (response: any) => {

                let responseData: any = response;
                if (responseData.statusCode == 200) {
                    this._baseService.isLoader = false;
                    this.InitializeForm();
                    this.dismissModalAfterAction();
                    let responseObj = responseData.data;
                    if (responseObj)
                    this._baseService.getAllCategories();
                    this.resetMDBModal();
                    this._utilityService.showSuccess('Success!', "Category Created Successfully.");
                }
                else {
                    this._baseService.isLoader = false;
                    this._utilityService.showError('Error!', responseData.message);
                }
            },
            (error: any) => {
            });
        } else {
            this._baseService.isLoader = true;
            let requestURL = this._Config.API_Root + this._Config.UpdateCategory;
            requestURL = requestURL.replace("{catID}", this.selectedCategory.id);
            let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
            console.log('requestURL---------create Question', requestURL, headerOptions);
            this._baseService.doPUT(requestURL, {name: this.createCategoryForm.value.categoryName, description: this.createCategoryForm.value.description}, headerOptions)
            .subscribe(
                (response: any) => {
                    let responseData: any = response;
                    if (responseData.statusCode == 200) {
                        this.dismissModalAfterAction();
                        this._baseService.isLoader = false;
                        this._utilityService.showSuccess("Success!", "Category is updated successfully.");
                    }
                    else {
                        this._baseService.isLoader = false;
                        this._utilityService.showError('Error!', responseData.message);
                    }
                },
                (error: any) => {
                console.log("Rrror", error);
                }
            );
            
        }
    }
    
    formButtonsLabel() {
        if(this.isCategoryEdit ) {
            this.formButtonsDisplayName = 'Update Category';
            this.formsHeading = 'Update a Category'; 
        } else{
            this.formButtonsDisplayName = 'Create Category';
            this.formsHeading = 'Create Category'; 
        }
    }

    updateCategoryPanel() {
        this.createCategoryForm = new FormGroup({
            categoryName: new FormControl(this.selectedCategory.name, Validators.required),
            description: new FormControl(this.selectedCategory.description, Validators.required),
        });
    }

    resetMDBModal() {
        this.isCategoryAdd = false;
        this.isCategoryEdit = false;
        this.formButtonsLabel();
        this.InitializeForm();
    }
}
