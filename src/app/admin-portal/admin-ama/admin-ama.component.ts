import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
/**** Config File ***/
import { APIConfig } from 'src/app/apiconfig';
import { BaseService } from 'src/app/Services/base.service';
import { UtilityService } from 'src/app/utility.service';
import { FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';
import { async } from '@angular/core/testing';


@Component({
  selector: 'app-admin-ama',
  templateUrl: './admin-ama.component.html',
  styleUrls: ['./admin-ama.component.scss']
})
export class AdminAmaComponent implements OnInit {

	constructor(public _Config: APIConfig, private _baseService: BaseService, private _utilityService: UtilityService) {}

	amaList: any;
	defaultImage = 'assets/img/avator.png';
	@ViewChild('modalDismiss') modalDismiss: ElementRef;
	selectedAMA: any;
	form: FormGroup;
	formsHeading: any;
	isEdit: boolean = false;
	isAdd: boolean = false;
	formButtonsDisplayName = "Create Poll";
	public uploader: FileUploader;
	tempPreview: any = "";
	selectedCategory: any = null;

	ngOnInit() {
		this._baseService.allLatestCategories.unshift({id: null, name: 'Choose Category'});
		this.InitializeForm();
        this.getActiveAMAsList();
        this.uploader = new FileUploader({ method: "PUT", itemAlias: 'picture', authToken: 'Bearer ' + localStorage.getItem('userToken') });
		if (this._baseService.currentUser.isLogedIn) {
            /********** File Upload Configurations **********/
            this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
            this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      
              let responseData: any = JSON.parse(response);
              if (responseData.statusCode == 200) {
                this.tempPreview = "";
              }
              else {
                this._utilityService.showError('Error!', responseData.message);
              }
      
            };
      
        }
	}

	getActiveAMAsList() {
		this._baseService.isLoader = true;
		let requestURL = this._Config.API_Root + this._Config.activeAMAList;
		let headerOptions = this._baseService.commonHeaders("withBearer");
		this._baseService.doGET(requestURL, headerOptions)
			.subscribe(
			(response: any) => {
				this.amaList = response.data;
				this.amaList.activeAMAList = this.amaList.newly;
				this._baseService.isLoader = false;
				console.log('this.amaList', this.amaList);
			},
			(error: any) => {
				console.log("Rrror", error);
				this._baseService.isLoader = false;
			}
		);
	}

	selectAMA(ama?: any, typeOfAction?: any) {
		this.selectedAMA = ama;
		if(typeOfAction == 'isEdit') {
			this.isEdit = true;
			this.formButtonsLabel();
			this.updateAMAPanel();
		} else if( typeOfAction == 'isAdd'){
			this.formButtonsLabel();
			this.isAdd = true;
			this.isEdit = true;
		}
		console.log(this.isEdit, typeOfAction, ama);
	}

	removeAMA() {
        this._baseService.isLoader = true;
        let requestURL = this._Config.API_Root + this._Config.DeleteAMA;
        requestURL = requestURL.replace("{AMA_ID}", this.selectedAMA.id);
        let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
        console.log('requestURL---------create Question', requestURL, headerOptions);
        this._baseService.doDelete(requestURL, headerOptions)
        .subscribe(
            (response: any) => {
                let responseData: any = response;
                if (responseData.statusCode == 200) {
                    this.dismissModalAfterAction();
					this._baseService.isLoader = false;
					this.getActiveAMAsList();
                    this._utilityService.showSuccess("Success!", "AMA is removed successfully.");
                }
                else {
                    this._baseService.isLoader = false;
                    this._utilityService.showError('Error!', responseData.message);
                }
            },
            (error: any) => {
            console.log("Rrror", error);
            }
        );
	}
	
	amaAction() {
        if(this.isEdit && this.isAdd) {
            if (!this.tempPreview) {
                this._utilityService.showError('Error!', "Please add AMA image");
            } else if(! this.selectedCategory[0].id) {
                this._utilityService.showError('Error!', "Please Select Category");
                
            } else {
                let TagsList = [];
                    if (this.form.value.tags)
                        TagsList = this.form.value.tags.split(",");

                    let PostBody = {
                        title: this.form.value.title,
                        description:  this.form.value.description,
                        tags: TagsList,
                        category: this.selectedCategory[0].id,
                    };
                this._baseService.isLoader = true;
                let requestURL = this._Config.API_Root + this._Config.CreateAMA;

                let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
                this._baseService.doPOST(requestURL, PostBody, headerOptions)
                .subscribe(
                (response: any) => { 

                    let responseData: any = response;
                    if (responseData.statusCode == 200) {
                        this._baseService.isLoader = false;
                        this.InitializeForm();
                        this.dismissModalAfterAction();
                        let responseObj = responseData.data;
                        if (responseObj)
                        this.uploadPollImage(responseData.data.id);
                        this.getActiveAMAsList();
                        this.resetMDBModal();
                        this._utilityService.showSuccess('Success!', "AMAs Created Successfully.");
                    }
                    else {
                        this._baseService.isLoader = false;
                        this._utilityService.showError('Error!', responseData.message);
                    }
                },
                (error: any) => {
                    });

                }
            } else {
                this._baseService.isLoader = true;
                let selectedCategory;
                if(this.selectCategory) {
                    selectedCategory = this.selectedCategory[0].id;
                } else {
                    selectedCategory = this._baseService.allLatestCategories.filter(category => category.name == this.selectedAMA.category.name);

                }
                console.log('selectedCategory', this.selectCategory, selectedCategory);
                let TagsList = [];
                    if (this.form.value.tags)
                        TagsList = this.form.value.tags.split(",");

                    let PostBody = {
                        title: this.form.value.title,
                        description:  this.form.value.description,
                        tags: TagsList,
                        category: selectedCategory[0].id,
                    };
                this._baseService.isLoader = true;
                let requestURL = this._Config.API_Root + this._Config.UpdateAMA;
                requestURL = requestURL.replace("{AMA_ID}", this.selectedAMA.id);
                let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
                this._baseService.doPUT(requestURL, PostBody, headerOptions)
                .subscribe(
                    (response: any) => {
                        let responseData: any = response;
                    if (responseData.statusCode == 200) {
                        this._baseService.isLoader = false;
                        this.InitializeForm();
                        this.dismissModalAfterAction();
                        let responseObj = responseData.data;
                        if (responseObj) {
                            this.getActiveAMAsList();
                            this.resetMDBModal();
                            this._utilityService.showSuccess('Success!', "AMA updated Successfully.");
                            if(this.tempPreview) {
                                this.uploadPollImage(responseData.data.id);
                            }
                        }
                        
                    } 
                    else {
                        this._baseService.isLoader = false;
                        this._utilityService.showError('Error!', responseData.message);
                    }
                    },
                    (error: any) => {
                        console.log("Rrror", error);
                    }
                );
        }
    }


	formButtonsLabel() {
		if(this.isEdit) {
			this.formButtonsDisplayName = 'Update AMA';
			this.formsHeading = 'Update a AMA'; 
		} else {
			this.formButtonsDisplayName = 'Create AMA';
			this.formsHeading = 'Create AMA'; 
		}
	}

	updateAMAPanel() {
		console.log('this.selectedPolls.--------------', this.selectedAMA);
        let firstTaG = this.selectedAMA.tags[0];
        let Tags:string;
        this.selectedAMA.tags.forEach(function(tag,index) {
            if(index == 0) {
                Tags= firstTaG;
            } else {
                Tags= Tags + ',' + tag;
            }
            console.log('tag', tag, index);

        });
        console.log('Tags-[------', Tags);
        this.form = new FormGroup({
            title: new FormControl(this.selectedAMA.title, Validators.required),
            description: new FormControl(this.selectedAMA.description, Validators.required),
            tags: new FormControl(Tags, Validators.required),
            category: new FormControl(this.selectedAMA.category.name, Validators.required),
        });
	}

	//Category Change event
	selectCategory(selectedCategory: any) {
		if (selectedCategory == "Choose Category") {
			this.selectedCategory = null;
		}
		else{
			this.selectedCategory = this._baseService.allLatestCategories.filter(category => category.name == selectedCategory.target.value);
		}
		console.log('selectedCategory',this.selectedCategory, selectedCategory.target.value);

	}

	/**
	 * Close Model after Actions Completed
	 */
	dismissModalAfterAction() {
		let el: HTMLElement = this.modalDismiss.nativeElement as HTMLElement;
		el.click();
	}

	//Initialize Form
	InitializeForm() {
		this.form = new FormGroup({
			title: new FormControl('', Validators.required),
			category: new FormControl('Choose Category', Validators.required),
			description: new FormControl('', Validators.required),
			tags: new FormControl('', Validators.required),
		});
	}

		/**
	 * To Initialize and Upload Picture
	 */
	uploadPollImage(PollID: any) {

		let requestURL = this._Config.API_Root + this._Config.UpdateAMAPicture;
		requestURL = requestURL.replace("{AMA_ID}", PollID);

		/********** File Upload Configurations **********/
		let apiURL = requestURL;

		// Update uploader URL
		this.uploader.setOptions({ method: "PUT", url: apiURL, itemAlias: 'picture', authToken: 'Bearer ' + localStorage.getItem('userToken') });

		// Clear the item queue (somehow they will upload to the old URL)
		// this.uploader.clearQueue();

		this.uploader.uploadAll();
	}

	resetMDBModal() {
        this.isAdd = false;
        this.isEdit = false;
        this.formButtonsLabel();
        this.InitializeForm();
        this.selectedCategory = null;
        this.tempPreview = null;
        this.selectedAMA = null;

    }
    
    onFileChange(event) {
        console.log('event------------------', event);
        let self = this;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
          var file: File = event.target.files[0];
          var myReader: FileReader = new FileReader();
    
          myReader.onloadend = (e) => {
            self.tempPreview = myReader.result;
          }
          myReader.readAsDataURL(file);
        }
    }

}
