import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAmaComponent } from './admin-ama.component';

describe('AdminAmaComponent', () => {
  let component: AdminAmaComponent;
  let fixture: ComponentFixture<AdminAmaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAmaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAmaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
