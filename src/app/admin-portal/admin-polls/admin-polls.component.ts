import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'; 
import { FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';

/**** Config File ***/
import { APIConfig } from 'src/app/apiconfig';

import { BaseService } from 'src/app/Services/base.service';
import { UtilityService } from 'src/app/utility.service';

import { async } from '@angular/core/testing';

@Component({
  selector: 'app-admin-polls',
  templateUrl: './admin-polls.component.html',
  styleUrls: ['./admin-polls.component.scss']
})
export class AdminPollsComponent implements OnInit {
	@ViewChild('modalDismiss') modalDismiss: ElementRef;
	selectedPolls: any;
	form: FormGroup;
	formsHeading: any;
	isEdit: boolean = false;
	isAdd: boolean = false;
	formButtonsDisplayName = "Create Poll";
    defaultImage = 'assets/img/touch.jpg';
    public uploader: FileUploader;
    tempPreview: any = "";
    selectedCategory: any = null;


	constructor(public _Config: APIConfig, private _baseService: BaseService, private _utilityService: UtilityService) {}

	ngOnInit() {
        this._baseService.allLatestCategories.unshift({id: null, name: 'Choose Category'});
        this.uploader = new FileUploader({ method: "PUT", itemAlias: 'picture', authToken: 'Bearer ' + localStorage.getItem('userToken') });
		this.InitializeForm();
		this._baseService.getActivePolls();
		setTimeout(() => {
		console.log(this._baseService.LatestActivePolls);
        }, 2000);
        if (this._baseService.currentUser.isLogedIn) {
            /********** File Upload Configurations **********/
            this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
            this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      
              let responseData: any = JSON.parse(response);
              if (responseData.statusCode == 200) {
                this.tempPreview = "";
              }
              else {
                this._utilityService.showError('Error!', responseData.message);
              }
      
            };
      
        }
	}
	removePolls() {
        this._baseService.isLoader = true;
        let requestURL = this._Config.API_Root + this._Config.DeletePollById;
        requestURL = requestURL.replace("{PollID}", this.selectedPolls.id);
        let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
        console.log('requestURL---------create Question', requestURL, headerOptions);
        this._baseService.doDelete(requestURL, headerOptions)
        .subscribe(
            (response: any) => {
                let responseData: any = response;
                if (responseData.statusCode == 200) {
                    this.dismissModalAfterAction();
                    this._baseService.isLoader = false;
		            this._baseService.getActivePolls();
                    this._utilityService.showSuccess("Success!", "Poll is removed successfully.");
                }
                else {
                    this._baseService.isLoader = false;
                    this._utilityService.showError('Error!', responseData.message);
                }
            },
            (error: any) => {
            console.log("Rrror", error);
            }
        );
    }

   /**
   * Close Model after Actions Completed
   */
    dismissModalAfterAction() {
        let el: HTMLElement = this.modalDismiss.nativeElement as HTMLElement;
        el.click();
    }

    selectPoll(poll?: any, typeOfAction?: any) {
        this.selectedPolls = poll;
        if(typeOfAction == 'isEdit') {
            this.isEdit = true;
            this.formButtonsLabel();
            this.updatePollPanel();
        } else if( typeOfAction == 'isAdd'){
            this.formButtonsLabel();
            this.isAdd = true;
            this.isEdit = true;
        }
        console.log(this.isEdit, typeOfAction, poll);
    }

    //Category Change event
    selectCategory(selectedCategory: any) {
        if (selectedCategory == "Choose Category") {
          this.selectedCategory = null;
        }
        else{
          this.selectedCategory = this._baseService.allLatestCategories.filter(category => category.name == selectedCategory.target.value);
        }
        console.log('selectedCategory',this.selectedCategory, selectedCategory.target.value);

    }

    //Initialize Form
	InitializeForm() {
        this.form = new FormGroup({
            title: new FormControl('', Validators.required),
            category: new FormControl('Choose Category', Validators.required),
            description: new FormControl('', Validators.required),
            tags: new FormControl('', Validators.required),
        });
    }

      /**
   * To Initialize and Upload Picture
   */
  uploadPollImage(PollID: any) {

    let requestURL = this._Config.API_Root + this._Config.UpdatePollPictureById;
    requestURL = requestURL.replace("{PollID}", PollID);

    /********** File Upload Configurations **********/
    let apiURL = requestURL;

    // Update uploader URL
    this.uploader.setOptions({ method: "PUT", url: apiURL, itemAlias: 'picture', authToken: 'Bearer ' + localStorage.getItem('userToken') });

    // Clear the item queue (somehow they will upload to the old URL)
    // this.uploader.clearQueue();

    this.uploader.uploadAll();
  }
    
    
	
	pollAction() {
        if(this.isEdit && this.isAdd) {
            if (!this.tempPreview) {
                this._utilityService.showError('Error!', "Please add Polls image");
            } else if(! this.selectedCategory[0].id) {
                this._utilityService.showError('Error!', "Please Select Category");
                
            } else {
                let TagsList = [];
                    if (this.form.value.tags)
                        TagsList = this.form.value.tags.split(",");

                    let PostBody = {
                        title: this.form.value.title,
                        description:  this.form.value.description,
                        tags: TagsList,
                        category: this.selectedCategory[0].id,
                    };
                this._baseService.isLoader = true;
                let requestURL = this._Config.API_Root + this._Config.CreatePoll;

                let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
                this._baseService.doPOST(requestURL, PostBody, headerOptions)
                .subscribe(
                (response: any) => { 

                    let responseData: any = response;
                    if (responseData.statusCode == 200) {
                        this._baseService.isLoader = false;
                        this.InitializeForm();
                        this.dismissModalAfterAction();
                        let responseObj = responseData.data;
                        if (responseObj)
                        this.uploadPollImage(responseData.data.id);
                        this._baseService.getActivePolls();
                        this.resetMDBModal();
                        this._utilityService.showSuccess('Success!', "Polls Created Successfully.");
                    }
                    else {
                        this._baseService.isLoader = false;
                        this._utilityService.showError('Error!', responseData.message);
                    }
                },
                (error: any) => {
                    });

                }
            } else {
                this._baseService.isLoader = true;
                let selectedCategory;
                if(this.selectCategory) {
                    selectedCategory = this.selectedCategory[0].id;
                } else {
                    selectedCategory = this._baseService.allLatestCategories.filter(category => category.name == this.selectedPolls.category.name);

                }
                console.log('selectedCategory', this.selectCategory, selectedCategory);
                let TagsList = [];
                    if (this.form.value.tags)
                        TagsList = this.form.value.tags.split(",");

                    let PostBody = {
                        title: this.form.value.title,
                        description:  this.form.value.description,
                        tags: TagsList,
                        category: selectedCategory[0].id,
                    };
                this._baseService.isLoader = true;
                let requestURL = this._Config.API_Root + this._Config.UpdatePollById;
                requestURL = requestURL.replace("{PollID}", this.selectedPolls.id);
                let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
                this._baseService.doPUT(requestURL, PostBody, headerOptions)
                .subscribe(
                    (response: any) => {
                        let responseData: any = response;
                    if (responseData.statusCode == 200) {
                        this._baseService.isLoader = false;
                        this.InitializeForm();
                        this.dismissModalAfterAction();
                        let responseObj = responseData.data;
                        if (responseObj) {
                            this._baseService.getActivePolls();
                            this.resetMDBModal();
                            this._utilityService.showSuccess('Success!', "Polls updated Successfully.");
                            if(this.tempPreview) {
                                this.uploadPollImage(responseData.data.id);
                            }
                        }
                        
                    }
                    else {
                        this._baseService.isLoader = false;
                        this._utilityService.showError('Error!', responseData.message);
                    }
                    },
                    (error: any) => {
                        console.log("Rrror", error);
                    }
                );
        }
    }
    
    formButtonsLabel() {
        if(this.isEdit) {
            this.formButtonsDisplayName = 'Update Poll';
            this.formsHeading = 'Update a Poll';
        } else {
            this.formButtonsDisplayName = 'Create Poll';
            this.formsHeading = 'Create Poll'; 
        }
    }

    updatePollPanel() {
        console.log('this.selectedPolls.--------------', this.selectedPolls);
        let firstTaG = this.selectedPolls.tags[0];
        let Tags:string;
        this.selectedPolls.tags.forEach(function(tag,index) {
            if(index == 0) {
                Tags= firstTaG;
            } else {
                Tags= Tags + ',' + tag;
            }
            console.log('tag', tag, index);

        });
        console.log('Tags-[------', Tags);
        this.form = new FormGroup({
            title: new FormControl(this.selectedPolls.title, Validators.required),
            description: new FormControl(this.selectedPolls.description, Validators.required),
            tags: new FormControl(Tags, Validators.required),
            category: new FormControl(this.selectedPolls.category.name, Validators.required),
        });
    }

    resetMDBModal() {
        this.isAdd = false;
        this.isEdit = false;
        this.formButtonsLabel();
        this.InitializeForm();
        this.selectedCategory = null;
        this.tempPreview= null;
        this.selectedPolls = null;

    }
    
    onFileChange(event) {
        console.log('event------------------', event);
        let self = this;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
          var file: File = event.target.files[0];
          var myReader: FileReader = new FileReader();
    
          myReader.onloadend = (e) => {
            self.tempPreview = myReader.result;
          }
          myReader.readAsDataURL(file);
        }
    }
}
