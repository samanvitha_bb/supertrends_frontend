import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from "@angular/router";

/**** Config File ***/
import { APIConfig } from '../../apiconfig';

/**** Base Service File ***/
import { BaseService } from '../../Services/base.service';
import { UtilityService } from '../../utility.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  userLoginForm: FormGroup;

  constructor(public _Config: APIConfig, public _baseService: BaseService,
    private _utilityService: UtilityService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {

    /**** To Set Login Fields Validations ***/
    this.userLoginForm = new FormGroup({
      Email: new FormControl('', Validators.required),
      Password: new FormControl('', Validators.required),
    });

  }


  /**
   * On Submit to Login as User and Redirect to Dashboard
   */
  Login() {

    /********** To Set Login Base Header Auth Token *************/
    var str = this.userLoginForm.value.Email + ":" + this.userLoginForm.value.Password;
    let base64Str = window.btoa(str);
    localStorage.setItem('userToken', base64Str);

    let PostBody = {
      access_token: this._Config.MasterKey
    };

    let requestURL = this._Config.API_Root + this._Config.makeLogin;
    let headerOptions = this._baseService.commonHeaders("withUserToken");
    this._baseService.doPOST(requestURL, PostBody, headerOptions)
      .subscribe(
        (result: any) => {

          if (result.statusCode == 200) {
            let data: any = result.data;

            data.user.isLogedIn = true;

            if (data.user.role == 'admin') {
              data.user.isAdmin = true;

              if (data.user.email == "ahmermunir@gmail.com") {
                data.user.isRootAdmin = true;
              }

            }
            else
            {
              data.user.isAdmin = false;
            }

            localStorage.setItem('userToken', data.token);
            localStorage.setItem('currentUser', JSON.stringify(data.user));
            this._baseService.currentUser = data.user;

            this._utilityService.showInfo('Welcome ' + data.user.firstName, 'You have loged in successfully.');
            this.router.navigate(['admin/dashboard']);

          }
          else {
            this._utilityService.showError('Error!', result.message);


          }

        },
        (error: any) => {
          console.log("Rrror", error);
        }
      );


  }



}
