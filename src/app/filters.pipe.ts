import { Pipe, PipeTransform } from '@angular/core';


/******************************************
Array | filters:searchTerm
******************************************/
@Pipe({
  name: 'filters'
})
export class FiltersPipe implements PipeTransform {

  transform(itemList: any, searchKeyword: string) {
    if (!itemList)
      return [];
    if (!searchKeyword)
      return itemList;
    let filteredList = [];
    if (itemList.length > 0) {
      searchKeyword = searchKeyword.toLowerCase();
      itemList.forEach(item => {
        let propValueList = Object.values(item);
        for (let i = 0; i < propValueList.length; i++) {
          if (propValueList[i]) {
            if (propValueList[i].toString().toLowerCase().indexOf(searchKeyword) > -1) {
              filteredList.push(item);
              break;
            }
          }
        }
      });
    }
    return filteredList;
  }

}



/******************************************
Array | filters:searchTerm
******************************************/
@Pipe({
  name: 'categoryFilters'
})
export class CategoryFiltersPipe implements PipeTransform {

  transform(itemList: any, searchKeyword: string) {
    if (!itemList)
      return [];
    if (!searchKeyword)
      return itemList;
    let filteredList = [];
    if (itemList.length > 0) {
      searchKeyword = searchKeyword.toLowerCase();
      itemList.forEach(item => {
        

        if (searchKeyword == "all") {
          filteredList.push(item);
        }
        else {
          if (item.category) {
            if (item.category.name.toString().toLowerCase().indexOf(searchKeyword) > -1) {
              filteredList.push(item);
            }
          }
        }


      });
    }
    return filteredList;
  }

}



/******************************************
Array | reverse 
******************************************/
@Pipe({
  name: 'reverse'
})
export class ReversePipe implements PipeTransform {
  transform(arr) {
    // var copy = arr.slice();
    // return arr.reverse();

    return arr;
  }
}


/******************************************
Array | customfilter:'key1,key2':searchTerm
******************************************/
@Pipe({
  name: 'customfilter',
  pure: false
})
export class CustomFilterPipe implements PipeTransform {

  public transform(value: any, keys: string, term: any) {

    if (!term) return value;

    return (value || []).filter((item: any) => keys.split(',').some(key => item.hasOwnProperty(key)));

  }
}



/******************************************
Array | orderBy : ObjFieldName: OrderByType
OrderByType = True or False
******************************************/

@Pipe({
  name: 'orderBy'
})
export class OrderByPipe implements PipeTransform {
  transform(items: any[], field: string, reverse: boolean = false): any[] {

    if (!items) return [];

    if (field) items.sort((a, b) => a[field] > b[field] ? 1 : -1);
    else items.sort((a, b) => a > b ? 1 : -1);

    if (reverse) items.reverse();

    return items;
  }
}

/******************************************
Array | orderByDate : ObjFieldName: OrderByType
OrderByType = True or False
******************************************/

@Pipe({
  name: 'orderByDate'
})
export class orderByDate implements PipeTransform {
  transform(items: any[], field: string, reverse: boolean): any[] {

    if (!items) return [];

    if (field) items.sort((a, b) => a[field] > b[field] ? 1 : -1);
    else items.sort((a, b) => a > b ? 1 : -1);

    if (reverse) items.reverse();
    return items;
  }
}