import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class APIConfig {

    /********* Root API URI ***********/
    //API_Root: string = "http://ec2-54-219-158-225.us-west-1.compute.amazonaws.com/api/v1";
    API_Root: string = "https://dev.supertrends.us/api/v1";
    // Root_Domain_Share: string = "http://localhost:4200/launch";
    Root_Domain_Share: string = "https://dev.supertrends.us/launch";
    MasterKey: string = "imrjvLDG4whKxUZUF8uh3jmg3hyLOGYd";

    /********* Signin / Signup ***********/
    makeRegister: string = "/register";
    makeLogin: string = "/authenticate";
    makeFacebookLogin: string = "/authenticate/facebook";
    makeGoogleLogin: string = "/authenticate/google";

    /********* Users ***********/
    CreateUsers: string = "/users";
    DeleteUsers: string = "/users/{UserID}";
    DisableUsers: string = "/users/{UserID}/disable";
    EnableUsers: string = "/users/{UserID}/enable";
    getUserDetailById: string = "/users/{UserID}";
    getAllUserList: string = "/users";
    UpdateUser: string = "/users/{UserID}";
    UpdateUserPassword: string = "/users/{UserID}";

    /********* Settings ***********/
    getSetting: string = "/settings/supertrends";
    putSetting: string = "/settings/supertrends";

    /********* Profile ***********/
    getProfile: string = "/profile";
    UpdateProfile: string = "/profile/update";
    UpdateProfilePicture: string = "/profile/picture";
    UpdateProfilePassword: string = "/profile/password";

    /********* Dashboard ***********/
    get_dashboard: string = "/dashboard";
    get_predictions: string = "/dashboard/predictions";

    /********* Home ***********/
    get_conseses: string = "/home/conseses";
    get_featuredpolls: string = "/home/featuredpolls";
    get_poll: string = "/home/poll/dsds";
    get_timeline: string = "/home/featuredpolls";

    /********* PreLaunch ***********/
    preLaunchUser: string = "/pre-launch-user";
    preLaunchMyShared: string = "/pre-launch-user/myshared";

    /********* Category ***********/
    getAllCategories: string = "/category";
    getAllActiveCategoryList: string = "/category/active";
    getAllCategoryListAMA: string = "/category/AMA/list";
    getSelectedCategory: string = "/category/{catID}";
    CreateCategory: string = "/category";
    DeleteCategory: string = "/category/{catID}";
    UpdateCategory: string = "/category/{catID}";

    /********* Question ***********/
    CreateQuestion: string = "/question";
    DeleteQuestion: string = "/question/{QuestionID}";
    UpdateQuestion: string = "/question/{QuestionID}";
    getAllQuestion: string = "/question";
    getQuestionById: string = "/question/{QuestionID}";
    getActiveQuestionList: string = "/question/active";
    getAllMyQuestionList: string = "/question/myquestion";
    UpdateQuestionPictureById: string = "/question/{QuestionID}/picture";

    UpdateQuestionCommentById: string = "/question/{QuestionID}/comment";
    GetQuestionCommentById: string = "/question/{QuestionID}/comments";
    LikeQuestionById: string = "/question/{QuestionID}/like";
    UpdateQuestionCommentReplyByCommentId: string = "/question/{QuestionID}/comment/{CommentID}/reply";
    GetQuestionCommentReplyByCommentId: string = "/question/{QuestionID}/comment/{CommentID}/replies";
    UpdateQuestionCommentLikeByCommentId: string = "/question/{QuestionID}/comment/{CommentID}/like";


    /********* Posts ***********/
    CreatePost: string = "/post";
    DeletePostById: string = "/post/{PostID}";
    UpdatePostById: string = "/post/{PostID}";
    getAllPostList: string = "/post";
    getAllMyPostList: string = "/post/mypost";
    getActivePostList: string = "/post/active";
    getPostByID: string = "/post/{PostID}";
    UpdatePostPictureById: string = "/post/{PostID}/picture";
    UpdatePostCommentById: string = "/post/{PostID}/comment";
    GetPostCommentById: string = "/post/{PostID}/comments";
    LikePostById: string = "/post/{PostID}/like";
    PredictPostById: string = "/post/{PostID}/prediction";
    GetMyPostPredictionById: string = "/post/{PostID}/prediction";
    UpdatePostCommentReplyByCommentId: string = "/post/{PostID}/comment/{CommentID}/reply";
    GetPostCommentReplyByCommentId: string = "/post/{PostID}/comment/{CommentID}/replies";
    UpdatePostCommentLikeByCommentId: string = "/post/{PostID}/comment/{CommentID}/like";

    /********* Polls ***********/
    CreatePoll: string = "/poll";
    DeletePollById: string = "/poll/{PollID}";
    UpdatePollById: string = "/poll/{PollID}";
    getAllActivePollList: string = "/poll/active";
    getAllMyPollList: string = "/poll/mypoll";
    getAllPollsList: string = "/poll";
    getPollByID: string = "/poll/{PollID}";
    UpdatePollPictureById: string = "/poll/{PollID}/picture";
    UpdatePollCommentById: string = "/poll/{PollID}/comment";
    GetPollCommentById: string = "/poll/{PollID}/comments";
    LikePollById: string = "/poll/{PollID}/like";
    PredictPollById: string = "/poll/{PollID}/prediction";
    GetMyPollPredictionById: string = "/poll/{PollID}/prediction";




    /********* AMA ***********/
    CreateAMA: string = "/ama";
    getAMAById: string = "/ama/{AMA_ID}";
    DeleteAMA: string = "/ama/{AMA_ID}";
    UpdateAMA: string = "/ama/{AMA_ID}";
    AssignAMA: string = "/ama/{AMA_ID}/assign";
    DeAssignAMA: string = "/ama/{AMA_ID}/deassign";
    AssignedamaList: string = "/ama/myassignedama";
    activeAMAList: string = "/ama/active";
    addQuestionAMA: string = "/ama/{AMA_ID}/addquestion";
    getAllAMAList: string = "/ama";
    getAMAQuestions: string = "/ama/{AMA_ID}/questions";
    answerquestionAMA: string = "/ama/{AMA_ID}/questions/{QUES_ID}/answer";
    commentquestionAMA: string = "/ama/5bba289cece6bd4ff65ecd5a/questions/5bba2bff603d765051b85832/comment";
    likequestionAMA: string = "/ama/{AMA_ID}/questions/{QUES_ID}/like";
    getAllmyAMAList: string = "/ama/myama";
    UpdateAMAPicture: string = "/ama/{AMA_ID}/picture";
    likeQuestionBYId: string = "/ama/{AMA_ID}/questions/{QUES_ID}/like";
    likeAMA :string = "/ama/{AMA_ID}/like";


     /********* Notifications ***********/
     GetAllNotifications: string = "/notification";
     GetLatestNotifications: string = "/notification/latest/{TimestampData}";
     
      /********* Notifications ***********/
      GetAllCountries: string = "./assets/json/countries.json";
      GetStatesByCountryID: string = "./assets/json/states.json";
      GetCitiesByStateID: string = "./assets/json/cities.json";
     
      
    constructor() { }

}