import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from "@angular/router";

/**** Social Signin/Signup Services ***/
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { GoogleLoginProvider, FacebookLoginProvider, LinkedInLoginProvider } from 'angularx-social-login';

/**** Config File ***/
import { APIConfig } from '../apiconfig';

/**** Base Service File ***/
import { BaseService } from '../Services/base.service';
import { UtilityService } from '../utility.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @ViewChild('closeButton') closeButton: ElementRef;
  userRegistrationForm: FormGroup;
  userLoginForm: FormGroup;
  user: SocialUser;

  actionButton;
  action = null;

  constructor(public _Config: APIConfig, public _baseService: BaseService,
    private _utilityService: UtilityService,
    private authService: AuthService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {

    // this._baseService.getAllCountries();

    this.initForms();
    this._baseService.getAllCategories();

    if (this._baseService.currentUser.isLogedIn) {
      setTimeout(() => {
        this._baseService.getActiveNotifications();
      }, 500);
    }

    
  }

  /**
   * Initialize form for User Registration and Login
   */
  initForms() {

    /**** To Set Registration Fields Validations ***/
    this.userRegistrationForm = new FormGroup({
      FirstName: new FormControl('', Validators.required),
      LastName: new FormControl('', Validators.required),
      Email: new FormControl('', Validators.required),
      Password: new FormControl('', Validators.required),
      CPassword: new FormControl('', Validators.required),
    });

    /**** To Set Login Fields Validations ***/
    this.userLoginForm = new FormGroup({
      Email: new FormControl('', Validators.required),
      Password: new FormControl('', Validators.required),
    });
  }


  /**
   * On Submit to Register as User and Redirect to Dashboard
   */
  onUserRegistrationFormSubmit() {

    let PostBody = {
      firstName: this.userRegistrationForm.value.FirstName,
      lastName: this.userRegistrationForm.value.LastName,
      email: this.userRegistrationForm.value.Email,
      password: this.userRegistrationForm.value.Password,
      access_token: this._Config.MasterKey
    };

    let requestURL = this._Config.API_Root + this._Config.makeRegister;
    let headerOptions = this._baseService.commonHeaders("");
    this._baseService.doPOST(requestURL, PostBody, headerOptions)
      .subscribe(
        (result: any) => {

          if (result.statusCode == 200) {
            let data: any = result.data;
            this.setUserProfile(data);
          }
          else {
            alert(result.message);
            console.log("POST Request is successful ", result);

          }

        },
        (error: any) => {
          console.log("Rrror", error);
        }
      );

  }


  /**
   * On Submit to Login as User and Redirect to Dashboard
   */
  onUserLoginFormSubmit() {

    /********** To Set Login Base Header Auth Token *************/
    var str = this.userLoginForm.value.Email + ":" + this.userLoginForm.value.Password;
    let base64Str = window.btoa(str);
    localStorage.setItem('userToken', base64Str);

    let PostBody = {
      access_token: this._Config.MasterKey
    };

    let requestURL = this._Config.API_Root + this._Config.makeLogin;
    let headerOptions = this._baseService.commonHeaders("withUserToken");
    this._baseService.doPOST(requestURL, PostBody, headerOptions)
      .subscribe(
        (result: any) => {

          if (result.statusCode == 200) {
            let data: any = result.data;

            this.setUserProfile(data);

          }
          else {
            // alert(result.message);
            // console.log("POST Request is successful ", result);
            this._utilityService.showError('Error!', result.message);


          }

        },
        (error: any) => {
          console.log("Rrror", error);
        }
      );


  }


  /**
   * To set current user Profile Info
   * @param data  //Response Data after login/Register
   */
  setUserProfile(data: any) {
    data.user.isLogedIn = true;

    if (data.user.role == 'admin')
      data.user.isAdmin = true;
    else
      data.user.isAdmin = false;

    localStorage.setItem('userToken', data.token);
    localStorage.setItem('currentUser', JSON.stringify(data.user));
    this._baseService.currentUser = data.user;
    this.triggerFalseClick();

    this._utilityService.showInfo('Welcome ' + data.user.firstName, 'You have loged in successfully.');

  }


  /**
   * Close Model after Registration or Login Completed
   */
  triggerFalseClick() {
    let el: HTMLElement = this.closeButton.nativeElement as HTMLElement;
    el.click();
  }


  /**
   * To Clear all the Localstorage Information
   */
  Logout() {
    localStorage.clear();
    this._baseService.currentUser = {};
    this.initForms();
    this.signOut();
    this.router.navigate(['timeline']);
  }




  /********************* Social Signin/Signup *********************/

  authenticatedUser() {
    this.authService.authState.subscribe((user) => {
      this.user = user;
      debugger;
      if (this.user) {
        this.onUserSocialLogin();
      }


    });
  }


  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    this.authenticatedUser();
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
    this.authenticatedUser();
  }

  signOut(): void {
    this.authService.signOut();
  }


  onUserSocialLogin() {

    let PostBody = {
      access_token: this.user.authToken
    };

    let requestURL = "";
    if (this.user.provider == "FACEBOOK")
      requestURL = this._Config.API_Root + this._Config.makeFacebookLogin;
    else if (this.user.provider == "GOOGLE")
      requestURL = this._Config.API_Root + this._Config.makeGoogleLogin;


    let headerOptions = this._baseService.commonHeaders("");
    this._baseService.doPOST(requestURL, PostBody, headerOptions)
      .subscribe(
        (result: any) => {
          debugger;

          if (result.statusCode == 200) {
            let data: any = result.data;
            this.setUserProfile(data);
          }
          else {
            // alert(result.message);
            // console.log("POST Request is successful ", result);
            this._utilityService.showError('Error!', result.message);

          }




        },
        (error: any) => {
          console.log("Rrror", error);
        }
      );


  }




}
