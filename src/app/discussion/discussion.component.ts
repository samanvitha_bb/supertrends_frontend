import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-discussion',
  templateUrl: './discussion.component.html',
  styleUrls: ['./discussion.component.scss']
})
export class DiscussionComponent  {

    discussionData: any[] = [
      {
        'title': 'When will Tesla make its entry into the Indian market?',
        'topic' : 'trending',
        'category' : 'automobile /  cars electrical cars',
        'user': 'Bob Business',
        'userimg': '',
        'time' : '5  hours ago',
        'msgcount' : '32',
        'count' : '500',
        'cardbg' : ''
      },
      {
        'title': 'When will Tesla make its entry into the Indian market2?',
        'topic' : 'trending',
        'category' : 'automobile /  cars electrical cars',
        'user': 'Bob Business',
        'userimg': '',
        'time' : '7  hours ago',
        'msgcount' : '32',
        'count' : '490',
        'cardbg' : ''
      },
      {
        'title': 'When will Tesla make its entry into the Indian market3?',
        'topic' : 'trending',
        'category' : 'automobile /  cars electrical cars',
        'user': 'Bob Business',
        'userimg': '',
        'time' : '2 days ago',
        'msgcount' : '32',
        'count' : '567',
        'cardbg' : ''
      }
    ];
}
