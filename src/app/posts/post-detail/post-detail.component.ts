import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Routes, RouterModule, Router, ActivatedRoute } from "@angular/router";


/**** Config File ***/
import { APIConfig } from '../../apiconfig';

/**** Base Service File ***/
import { BaseService } from '../../Services/base.service';
import { UtilityService } from '../../utility.service';


@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss']
})
export class PostDetailComponent implements OnInit {

  @Input() selectedItem: any;
  @Input() type: any;
  @Output() isBack = new EventEmitter();

  selectedItemType: any = "";
  selectedItemInfo: any = {};
  postCommentInput: any = "";
  isOrderby: boolean = true;


  defaultImage = 'assets/img/touch.jpg';
  offset = 0;

  ReplyComment: any = "";
  selectedCommentReplies: any = [];

  constructor(public _Config: APIConfig,
    private _baseService: BaseService,
    private _utilityService: UtilityService,
    private router: Router, private route: ActivatedRoute) {


  }


  ngOnInit() {

    if (this.type == "Post")
      this.selectedItemType = "Posts";

    this.selectedItemInfo = this.selectedItem;

    if (this._baseService.currentUser.isLogedIn) {
      this.ViewDetailItem();
      this._baseService.getPostsCommentsByID(this.selectedItem.id);


      this._baseService.selectedPostComments.forEach(element => {
        element.isReply = false;
      });
    }

  }



  /**
   * Post Details page back button event and Emit the Boolean Value True
   * @param event 
   */
  goBack() {
    this.isBack.emit(true);
  }


  /**
   * To make Order by Ascending or Descending
   * @param type 
   */
  selectOrderBy(type: boolean) {
    this.isOrderby = type;
  }

  ViewDetailItem() {
    if (this._baseService.currentUser.isLogedIn) {

      let requestURL = this._Config.API_Root + this._Config.getPostByID;
      requestURL = requestURL.replace("{PostID}", this.selectedItem.id);

      let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
      this._baseService.doGET(requestURL, headerOptions)
        .subscribe(
          (response: any) => {

          },
          (error: any) => {
            console.log("Rrror", error);
          }
        );

    }
  }

  likeItem() {
    if (this._baseService.currentUser.isLogedIn) {

      let requestURL = this._Config.API_Root + this._Config.LikePostById;
      requestURL = requestURL.replace("{PostID}", this.selectedItem.id);

      let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
      this._baseService.doPUT(requestURL, {}, headerOptions)
        .subscribe(
          (response: any) => {
            let responseData: any = response;
            if (responseData.statusCode == 200) {

              let responseObj = responseData.data;
              if (responseObj) {

                if (responseObj.like_count == this.selectedItem.like_count) {
                  this._utilityService.showWarning('Warning!', "You have already liked this post!");
                }
                else {
                  this.selectedItem.like_count = responseObj.like_count;
                  this._utilityService.showSuccess('Success!', "Post Liked Successfully.");
                }

              }



            }
            else {
              this._utilityService.showError('Error!', responseData.message);
            }
          },
          (error: any) => {
            console.log("Rrror", error);
          }
        );

    }
  }


  postComment() {

    if (this._baseService.currentUser.isLogedIn) {

      let PostBody = {
        comment: this.postCommentInput,
      };

      this._baseService.isLoader = true;

      let requestURL = this._Config.API_Root + this._Config.UpdatePostCommentById;
      requestURL = requestURL.replace("{PostID}", this.selectedItem.id);

      let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
      this._baseService.doPUT(requestURL, PostBody, headerOptions)
        .subscribe(
          (response: any) => {

            let responseData: any = response;
            if (responseData.statusCode == 200) {

              this._baseService.isLoader = false;

              let responseObj = responseData.data;
              if (responseObj)
                this._baseService.getPostsCommentsByID(responseObj.id);

              this.postCommentInput = "";
              this._utilityService.showSuccess('Success!', "Comment Posted Successfully.");

              this.selectedItemInfo.comment_count = this.selectedItemInfo.comment_count + 1;

            }
            else {
              this._utilityService.showError('Error!', responseData.message);
            }

          },
          (error: any) => {
            console.log("Rrror", error);
          }
        );

    }
    else {
      this._utilityService.showWarning("", "Please Login and post your comment!");
    }


  }


  showReply(selectedItem) {
    this._baseService.selectedPostComments.forEach(element => {
      element.isReply = false;
    });

    let itemIndex = this._baseService.selectedPostComments.findIndex(obj => obj.id == selectedItem.id);
    this._baseService.selectedPostComments[itemIndex].isReply = true;
  }


  postReplyComment(selectedItem) {

    if (this._baseService.currentUser.isLogedIn) {

      let PostBody = {
        comment: this.ReplyComment,
      };

      this._baseService.isLoader = true;

      let requestURL = this._Config.API_Root + this._Config.UpdatePostCommentReplyByCommentId;
      requestURL = requestURL.replace("{PostID}", this.selectedItem.id);
      requestURL = requestURL.replace("{CommentID}", selectedItem.id);

      let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
      this._baseService.doPUT(requestURL, PostBody, headerOptions)
        .subscribe(
          (response: any) => {

            let responseData: any = response;
            if (responseData.statusCode == 200) {

              this._baseService.isLoader = false;

              let responseObj = responseData.data;
              if (responseObj)
                this._baseService.getPostsCommentsByID(this.selectedItem.id);

              this.postCommentInput = "";
              this._utilityService.showSuccess('Success!', "Comment Posted Successfully.");

              this.selectedItemInfo.comment_count = this.selectedItemInfo.comment_count + 1;

            }
            else {
              this._utilityService.showError('Error!', responseData.message);
            }

          },
          (error: any) => {
            console.log("Rrror", error);
          }
        );

    }
    else {
      this._utilityService.showWarning("", "Please Login and post your comment!");
    }


  }


  likeCommentItem(SelectedComment: any, type: any) {
    if (this._baseService.currentUser.isLogedIn) {

      let requestURL = this._Config.API_Root + this._Config.UpdatePostCommentLikeByCommentId;
      requestURL = requestURL.replace("{PostID}", this.selectedItem.id);
      requestURL = requestURL.replace("{CommentID}", SelectedComment.id);


      let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
      this._baseService.doPUT(requestURL, {}, headerOptions)
        .subscribe(
          (response: any) => {
            let responseData: any = response;
            if (responseData.statusCode == 200) {

              let responseObj = responseData.data;
              if (responseObj) {

                if (responseObj.like_count == SelectedComment.like_count) {
                  this._utilityService.showWarning('Warning!', "You have already liked this comment!");
                }
                else {

                  if (type == 1) {
                    let itemIndex = this._baseService.selectedPostComments.findIndex(obj => obj.id == SelectedComment.id);
                    this._baseService.selectedPostComments[itemIndex].like_count = responseObj.like_count;

                  }
                  // else {
                  //   let itemIndex = this._baseService.selectedPostComments.ReplyComments.findIndex(obj => obj.id == SelectedComment.id);
                  //   this._baseService.selectedPostComments[itemIndex].like_count = responseObj.like_count;

                  // }
                  this._utilityService.showSuccess('Success!', "Comment Liked Successfully.");
                }

              }

            }
            else {
              this._utilityService.showError('Error!', responseData.message);
            }
          },
          (error: any) => {
            console.log("Rrror", error);
          }
        );

    }
  }

  /**
 *To Get all Active Polls Comments List
 */
  async  showAllReplies(selectedItem: any) {

    let requestURL = this._Config.API_Root + this._Config.GetPostCommentReplyByCommentId;
    requestURL = requestURL.replace("{PostID}", this.selectedItem.id);
    requestURL = requestURL.replace("{CommentID}", selectedItem.id);

    let headerOptions = this._baseService.commonHeaders("withBearerUserToken");

    this._baseService.isLoader = true;


    await this._baseService.doGET(requestURL, headerOptions)
      .subscribe(
        (response: any) => {
          let result: any = response.data;
          debugger;
          this.selectedCommentReplies = result.rows;

          let itemIndex = this._baseService.selectedPostComments.findIndex(obj => obj.id == selectedItem.id);
          this._baseService.selectedPostComments[itemIndex].ReplyComments = result.rows;

          setTimeout(() => {
            this._baseService.isLoader = false;
          }, 500);


        },
        (error: any) => {
          console.log("Rrror", error);
          this._baseService.isLoader = false;

        }
      );
  }




}

