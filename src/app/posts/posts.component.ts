import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Routes, RouterModule, Router, ActivatedRoute } from "@angular/router";

/**** Config File ***/
import { APIConfig } from '../apiconfig';

/**** Base Service File ***/
import { BaseService } from '../Services/base.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  isEdit: boolean = false;
  showDetails: boolean = false;
  showCreateorUpdateForm: boolean = false;
  showReadMoreItems: boolean = false;
  selectedItem: any = {};
  selectedCategory: any = { id: "", name: "All" };

  SearchKeyword: any = "";
  isOrderby: boolean = true;

  ReadMoreType: any = "";

  defaultImage = 'assets/img/touch.jpg';
  offset = 0;

  constructor(public _Config: APIConfig, private _baseService: BaseService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {

    this._baseService.getActivePosts();

  }


  /**
   * View Selected Post Details
   * @param selectedItem 
   */
  ViewItem(selectedItem: any) {
    this.showDetails = true;
    this.selectedItem = selectedItem;
    window.scroll(0, 0);

  }


  /**
   * Post Details page back button event
   * @param event 
   */
  handleBackChange(event: any) {
    this.showDetails = !event;
    this.showCreateorUpdateForm = !event;
    this.ReadMoreType = !event;
    this._baseService.getActivePosts();

  }

  /**
   * To make Order by Ascending or Descending
   * @param type 
   */
  selectOrderBy(type: boolean) {
    this.isOrderby = type;
  }

  /*********  Category Change event  ***********/
  selectCategory(selectedCategory: any) {
    if (selectedCategory == "All") {
      this.selectedCategory = { id: "", name: "All" };
    }
    else{
      this.selectedCategory = selectedCategory;
    }
  }


  /**
   * Show or Hide Form by type
   * @param type 
   */
  showHideForm(type: boolean) {
    this.showCreateorUpdateForm = type;
  }


  /**
   * Redirect to Readmore page and set selected category
   * @param readType 
   */
  readMore(readType: any) {
    localStorage.setItem("readMore_Post", readType);
    this.router.navigate(['posts/readmore']);

  }
}

