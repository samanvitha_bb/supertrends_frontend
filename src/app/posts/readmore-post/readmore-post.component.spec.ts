import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadmorePostComponent } from './readmore-post.component';

describe('ReadmorePostComponent', () => {
  let component: ReadmorePostComponent;
  let fixture: ComponentFixture<ReadmorePostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadmorePostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadmorePostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
