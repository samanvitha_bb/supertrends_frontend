import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Routes, RouterModule, Router, ActivatedRoute } from "@angular/router";


/**** Config File ***/
import { APIConfig } from '../../apiconfig';

/**** Base Service File ***/
import { BaseService } from '../../Services/base.service';


@Component({
  selector: 'app-readmore-post',
  templateUrl: './readmore-post.component.html',
  styleUrls: ['./readmore-post.component.scss']
})
export class ReadmorePostComponent implements OnInit {

  isEdit: boolean = false;
  showDetails: boolean = false;
  showReadMoreItems: boolean = false;
  selectedItem: any = {};
  selectedCategory: any = {id:"",name:"All"};

  SearchKeyword: any = "";
  isOrderby: boolean = true;

  ReadMoreType: any = "";
  selectedFilterTitle: any = "";


  defaultImage = 'assets/img/touch.jpg';
  offset = 0;
  
  constructor(public _Config: APIConfig, private _baseService: BaseService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {

    this._baseService.getActivePosts();

    this.ReadMoreType = localStorage.getItem("readMore_Post");
    if (this.ReadMoreType) {

      //set Filter for selected Items here

      if (this.ReadMoreType == "Trending") {
        this.selectedFilterTitle = "Trending";
      }
      else if (this.ReadMoreType == "Newest") {
        this.selectedFilterTitle = "Newest";

      }
      else if (this.ReadMoreType == "Popular-Technology") {
        this.selectedFilterTitle = "Popular in Technology";

      }

      //-----------------

      this._baseService.getActivePosts();
    }
    else
      this.router.navigate(['/posts']);


  }


  /**
  * View Selected Post Details
  * @param selectedItem 
  */
  ViewItem(selectedItem: any) {
    this.showDetails = true;
    this.selectedItem = selectedItem;
    window.scroll(0, 0);

  }


  /**
   * Post Details page back button event
   * @param event 
   */
  handleBackChange(event: any) {
    this.showDetails = !event;
    this.ReadMoreType = !event;
    // this._baseService.getActivePosts();

  }


  /**
   * To make Order by Ascending or Descending
   * @param type 
   */
  selectOrderBy(type: boolean) {
    this.isOrderby = type;
  }


    /*********  Category Change event  ***********/
    selectCategory(selectedCategory: any) {
      if (selectedCategory == "All") {
        this.selectedCategory = { id: "", name: "All" };
      }
      else{
        this.selectedCategory = selectedCategory;
      }
    }

    
}

