import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter, TemplateRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Routes, RouterModule, Router, ActivatedRoute } from "@angular/router";
import { FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';


/**** Config File ***/
import { APIConfig } from '../../apiconfig';

/**** Base Service File ***/
import { BaseService } from '../../Services/base.service';
import { UtilityService } from '../../utility.service';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss']
})


export class CreatePostComponent implements OnInit {

  @Input() selectedItem: any;
  @Input() type: any;
  @Output() isBack = new EventEmitter();

  polls = [];
  itemCount: number;
  pollText: string = '';
  btnText: string = 'Add item';

  postFormObject: any = {};
  selectedCategory: string = "All";

  tags: any;


  selectedFile: File;
  allFilesList: any = [];
  public uploader: FileUploader;
  public apiURL: any = "";
  tempPreview: any = "";

  defaultImage = 'assets/img/touch.jpg';


  modalRef: BsModalRef;
  message: string;


  constructor(public _Config: APIConfig, private _baseService: BaseService,
    private _utilityService: UtilityService,
    private modalService: BsModalService,
    private router: Router, private route: ActivatedRoute) {


  }

  ngOnInit() {

    this.uploader = new FileUploader({ method: "PUT", itemAlias: 'picture', authToken: 'Bearer ' + localStorage.getItem('userToken') });

    if (this._baseService.currentUser.isLogedIn) {

      /********** File Upload Configurations **********/
      this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
      this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {

        let responseData: any = JSON.parse(response);
        if (responseData.statusCode == 200) {
          this.tempPreview = "";
          this.cancelCreatePost();
          this._utilityService.showSuccess('Success!', 'Post Created successfully.');
        }
        else {
          this._utilityService.showError('Error!', responseData.message);
          this._baseService.isLoader = false;

        }

      };

    }
  }


  /**
   *Set Category by Change event
   * @param selectedCategory 
   */
  selectCategory(selectedCategory: any) {
    this.selectedCategory = selectedCategory;
  }

 
  /**
   * On Submit To create a Post
   */
  onFormSubmit() {

    if (!this.tempPreview) {
      this._utilityService.showError('Error!', "Please add post image");
    }
    else {

      let TagsList = [];
      if (this.postFormObject.Tags)
        TagsList = this.postFormObject.Tags.split(",");

      let PostBody = {
        title: this.postFormObject.Title,
        description: this.postFormObject.Description,
        tags: TagsList,
        category: this.postFormObject.Category,
      };

      this._baseService.isLoader = true;

      let requestURL = this._Config.API_Root + this._Config.CreatePost;
      let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
      this._baseService.doPOST(requestURL, PostBody, headerOptions)
        .subscribe(
          (response: any) => {

            let responseData: any = response;
            if (responseData.statusCode == 200) {
              this.uploadPollImage(responseData.data.id);

            }
            else {
              this._utilityService.showError('Error!', responseData.message);
            }

          },
          (error: any) => {
            console.log("Rrror", error);
          }
        );

    }


  }



  /**
   * To Initialize and Upload Picture
   */
  uploadPollImage(PostID: any) {

    let requestURL = this._Config.API_Root + this._Config.UpdatePostPictureById;
    requestURL = requestURL.replace("{PostID}", PostID);

    /********** File Upload Configurations **********/
    this.apiURL = requestURL;
    // this.uploader = new FileUploader({ method: "PUT", url: this.apiURL, itemAlias: 'picture', authToken: 'Bearer ' + localStorage.getItem('userToken') });

    // Update uploader URL
    this.uploader.setOptions({ method: "PUT", url: this.apiURL, itemAlias: 'picture', authToken: 'Bearer ' + localStorage.getItem('userToken') });

    // Clear the item queue (somehow they will upload to the old URL)
    // this.uploader.clearQueue();

    this.uploader.uploadAll();
  }



  /**
   * cancel button event and Emit the Boolean Value True
   * @param event 
   */
  cancelCreatePost() {
    this.isBack.emit(true);
  }


  onFileChange(event) {
    let self = this;
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      var file: File = event.target.files[0];
      var myReader: FileReader = new FileReader();

      myReader.onloadend = (e) => {
        self.tempPreview = myReader.result;
      }
      myReader.readAsDataURL(file);


    }
  }



  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  confirm(): void {
    // this.votePoll();
    this.cancelCreatePost();
    this._utilityService.showSuccess('Success!', 'Post Created successfully.');

    // this.message = 'Confirmed!';
    this.modalRef.hide();
  }

  decline(): void {
    // this.cancelCreatePost();
    // this._utilityService.showSuccess('Success!', 'Poll Created successfully.');

    // this.message = 'Declined!';
    this.modalRef.hide();
  }


 

}
