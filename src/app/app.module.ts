import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {TimeAgoPipe} from 'time-ago-pipe';
import { ModalModule } from 'ngx-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { FileSelectDirective } from 'ng2-file-upload';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { TypeaheadModule } from 'ngx-bootstrap';
import {SelectDropDownModule} from 'ngx-select-dropdown';
import { TabsModule } from 'ngx-bootstrap';

import { BaseService } from './Services/base.service';
import { WeatherService } from './Services/weather.service';
import { PollService } from './Services/poll.service';

import { CountdownTimerModule } from 'ngx-countdown-timer';
import { LaunchComponent } from './launch/launch.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ChartsModule } from 'angular-bootstrap-md';
import { DiscussionComponent } from './discussion/discussion.component';
import { WavesModule } from 'angular-bootstrap-md';
import { QuestionsComponent } from './questions/questions.component';
import { QuestionDetailComponent } from './questions/question-detail/question-detail.component';
import { AmaComponent } from './ama/ama.component';
import { AmaDetailComponent } from './ama-detail/ama-detail.component';
import { ProfileComponent } from './profile/profile.component';
import { TimelineComponent } from './timeline/timeline.component';
import { VoteComponent } from './vote/vote.component';
import { NotificationComponent } from './notification/notification.component';
import { SearchComponent } from './search/search.component';
import { PollsComponent } from './polls/polls.component';
import { FiltersPipe, ReversePipe, CustomFilterPipe, OrderByPipe,orderByDate,CategoryFiltersPipe } from './filters.pipe';
import { PollDetailComponent } from './polls/poll-detail/poll-detail.component';
import { CreatePollComponent } from './polls/create-poll/create-poll.component';
import { ReadmorePollComponent } from './polls/readmore-poll/readmore-poll.component';
import { PostsComponent } from './posts/posts.component';
import { PostDetailComponent } from './posts/post-detail/post-detail.component';
import { ReadmorePostComponent } from './posts/readmore-post/readmore-post.component';
import { PreLoaderComponent } from './pre-loader/pre-loader.component';
import { SocialAuthComponent } from './social-auth/social-auth.component';
import { PollDetailsComponent } from './profile/poll-details/poll-details.component';


/******** Social Auth Config Files ******/
import { SocialLoginModule } from 'angularx-social-login';
import { AuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider, LinkedInLoginProvider } from 'angularx-social-login';
import { CreatePostComponent } from './posts/create-post/create-post.component';

/******** Admin Portal ******/
import { AdminPortalComponent } from './admin-portal/admin-portal.component';
import { LoginComponent } from './admin-portal/login/login.component';
import { AdminDashboardComponent } from './admin-portal/admin-dashboard/admin-dashboard.component';
import { CategoryComponent } from './admin-portal/category/category.component';
import { UsersComponent } from './admin-portal/users/users.component';
import { AdminPollsComponent } from './admin-portal/admin-polls/admin-polls.component';
import { AdminPostsComponent } from './admin-portal/admin-posts/admin-posts.component';
import { AdminAmaComponent } from './admin-portal/admin-ama/admin-ama.component';

const config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('751013629576-lgia3s1jab7s6jd43j881amhqhig5t0r.apps.googleusercontent.com')
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('734154663626925')
  },
  // {
  //   id: LinkedInLoginProvider.PROVIDER_ID,
  //   provider: new LinkedInLoginProvider('78iqy5cu2e1fgr')
  // }
]);

export function provideConfig() {
  return config;
}



@NgModule({
  declarations: [
    AppComponent,
    LaunchComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    DashboardComponent,
    DiscussionComponent,
    QuestionsComponent,
    QuestionDetailComponent,
    AmaComponent,
    AmaDetailComponent,
    ProfileComponent,
    TimelineComponent,
    VoteComponent,
    NotificationComponent,
    SearchComponent,
    PollsComponent,
    FiltersPipe,
    ReversePipe, CustomFilterPipe, OrderByPipe,orderByDate,CategoryFiltersPipe,
    TimeAgoPipe,
    PollDetailComponent,
    CreatePollComponent,
    ReadmorePollComponent,
    PostsComponent,
    PostDetailComponent,
    ReadmorePostComponent,
    PreLoaderComponent,
    SocialAuthComponent,
    FileSelectDirective,
    CreatePostComponent,
    PollDetailsComponent,
    AdminPortalComponent,
    LoginComponent,
    CategoryComponent,
    UsersComponent,
    AdminPollsComponent,
    AdminPostsComponent,
    AdminAmaComponent,
    AdminDashboardComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    CountdownTimerModule.forRoot(),
    HttpClientModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    SocialLoginModule,  // Social Signin/signup Module
    ModalModule.forRoot(),  // NGX Model Module
    BrowserAnimationsModule, // Animations Module
    ToastrModule.forRoot(), // ToastrModule Module
    BsDatepickerModule.forRoot(), //DatePicker Module
    TimepickerModule.forRoot(), //Timepicker Module
    LazyLoadImageModule,  //Lazy Load image Module
    TypeaheadModule.forRoot(),
    SelectDropDownModule,
    TabsModule.forRoot()
  ],
  providers: [BaseService, WeatherService, PollService,
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
