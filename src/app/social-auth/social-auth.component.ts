import { Component, OnInit } from '@angular/core';

import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { GoogleLoginProvider, FacebookLoginProvider, LinkedInLoginProvider } from 'angularx-social-login';


@Component({
  selector: 'app-social-auth',
  templateUrl: './social-auth.component.html',
  styleUrls: ['./social-auth.component.scss']
})
export class SocialAuthComponent implements OnInit {

  user: SocialUser;

  constructor(private authService: AuthService) {


  }

  ngOnInit() {



  }


  authenticatedUser() {
    this.authService.authState.subscribe((user) => {
      this.user = user;
      debugger;

      if (this.user) {
       
        if (this.user.provider == "GOOGLE") {

        }

        if (this.user.provider == "FACEBOOK") {

        }

      }


    });
  }


  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    this.authenticatedUser();
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
    this.authenticatedUser();
  }

  // signInWithLinkedIn(): void {
  //   this.authService.signIn(LinkedInLoginProvider.PROVIDER_ID);
  // }

  signOut(): void {
    this.authService.signOut();
  }



}
