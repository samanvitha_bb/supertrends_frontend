import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from "@angular/router";

/**** Social Signin/Signup Services ***/
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { GoogleLoginProvider, FacebookLoginProvider, LinkedInLoginProvider } from 'angularx-social-login';

/**** Config File ***/
import { APIConfig } from '../apiconfig';

/**** Base Service File ***/
import { BaseService } from '../Services/base.service';
import { UtilityService } from '../utility.service';


@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  isOrderby: boolean = true;

  constructor(public _Config: APIConfig, public _baseService: BaseService,
    private _utilityService: UtilityService,
    private authService: AuthService,
    private router: Router, private route: ActivatedRoute) { }


  ngOnInit() {


    if (this._baseService.currentUser.isLogedIn) {
      setTimeout(() => {
        this._baseService.getActiveNotifications();
      }, 500);

      this._baseService.getAllNotifications();

    }
    else {
      this.router.navigate(['timeline']);
    }
  }



  /**
   * To make Order by Ascending or Descending
   * @param type 
   */
  selectOrderBy(type: boolean) {
    this.isOrderby = type;
  }

}
