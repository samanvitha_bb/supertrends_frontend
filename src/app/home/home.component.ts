import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { WeatherService } from '../Services/weather.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  chart = []; // This will hold our chart info

  trendingData = [];
  constructor(private _weather: WeatherService) {}

  ngOnInit() {
    this._weather.dailyForecast().subscribe(res => {
      let temp_max = res['list'].map(res => res.main.temp_max);
      let temp_min = res['list'].map(res => res.main.temp_min);
      let pressure = res['list'].map(res => res.main.pressure);
      let sea_level = res['list'].map(res => res.main.sea_level);
      let humidity = res['list'].map(res => res.main.humidity);
      let alldates = res['list'].map(res => res.dt);

      let weatherDates = [];
      alldates.forEach(res => {
        let jsdate = new Date(res * 1000);
        weatherDates.push(
          jsdate.toLocaleTimeString('en', {
            year: 'numeric',
            month: 'short',
            day: 'numeric'
          })
        );
      });


      this.chart = new Chart('canvas', {
        type: 'bar',
        data: {
          labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May' , 'Jun' , 'Jul', 'Aug' , 'Sep', 'Oct', 'Nov', 'Dec'],
          datasets: [
            {
              data: temp_min,
              borderColor: 'rgb(42,144,255)',
              backgroundColor: 'rgba(42,144,255, 0.5)',
              fill: false
            },
          ]
        },
        options: {
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              display: true
            }],
            yAxes: [{
              display: true,
              ticks : {
                stepSize: 3,
              }
            }],
          }
        }
      });


      this.chart = new Chart('canvas2', {
        type: 'line',
        data: {
          labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May' , 'Jun' , 'Jul', 'Aug' ,
           'Sep', 'Oct', 'Nov', 'Dec', 'Jan', 'Feb', 'Mar', 'Apr', 'May' , 'Jun' ],
          datasets: [
            {
              data: humidity,
              borderColor: 'rgb(42,144,255)',
              backgroundColor: 'rgba(42,144,255, 0.5)',
              fill: false
            },
          ]
        },
        options: {
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              display: true
            }],
            yAxes: [{
              display: true,
              ticks : {
                stepSize: 10,
              }
            }],
          }
        }
      });



      this.chart = new Chart('canvas3', {
        type: 'doughnut',
        data: {
          labels: ['Jan', 'Feb'],
          datasets: [
            {
              data: ['73' , '27'],
              borderColor: ['rgb(42,144,255)', 'rgb(225,54,255)'],
              borderWidth: 0,
              backgroundColor: ['rgba(42,144,255, 0.5)', 'rgba(225,255,255, 0.5)'],
              animateRotate: true,
            },
          ]
        },
        options: {
          legend: {
            display: false
          },
          cutoutPercentage : 90,
        }
      });


      this.chart = new Chart('canvas4', {
        type: 'bar',
        data: {
          labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May' , 'Jun' ,
           'Jul', 'Aug' , 'Sep', 'Oct', 'Nov', 'Dec', 'Jan', 'Feb', 'Mar',
           'Apr', 'May' , 'Jun' , 'Jul', 'Aug' , 'Sep', 'Oct', 'Nov', 'Dec',
           'Jan', 'Feb'  ],
          datasets: [
            {
              data: temp_max,
              borderColor:  'rgb(225,54,255)',
              borderWidth: 0,
              backgroundColor: 'rgba(42,144,255,0.5)',
            },
          ]
        },
        options: {
          legend: {
            display: false
          },
          maintainAspectRatio: false,
          scales: {
            xAxes: [{
              display: true,
              ticks : {
                stepSize: 50,
              },
              gridLines: {
                color: 'rgba(0, 0, 0, 0)',
            }
            }],
            yAxes: [{
              display: false,
              ticks : {
                stepSize: 100,
                max: 310,
              },
            }],
            // maxBarThickness: 2,
            // max: 370,
          }
        }
      });



      this.trendingData = [
        {
          'title': 'When will Tesla make its entry into the Indian market?',
          'topic' : 'trending',
          'category' : 'automobile /  cars electrical cars',
          'user': 'Bob Business',
          'userimg': '',
          'time' : '5  hours ago',
          'msgcount' : '32',
          'count' : '500',
          'cardbg' : ''
        },
        {
          'title': 'When will Tesla make its entry into the Indian market3?',
          'topic' : 'trending',
          'category' : 'automobile /  cars electrical cars',
          'user': 'Bob Business',
          'userimg': '',
          'time' : '',
          'msgcount' : '32',
          'count' : '567',
          'cardbg' : ''
        },
        {
          'title': 'When will Tesla make its entry into the Indian market?',
          'topic' : 'trending',
          'category' : 'automobile /  cars electrical cars',
          'user': 'Bob Business',
          'userimg': '',
          'time' : '',
          'msgcount' : '32',
          'count' : '213',
          'cardbg' : ''
        }
      ];


    });
  }
}
