import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, pipe , BehaviorSubject } from 'rxjs';


/**** Config File ***/
import { APIConfig } from '../apiconfig';

@Injectable({
  providedIn: 'root'
})
export class PollService {

  private polls = new BehaviorSubject<any>([]);
  poll = this.polls.asObservable();

  constructor(public _Config: APIConfig, public _http: HttpClient) { }

  changePoll(poll) {
    this.polls.next(poll);
  }





}
