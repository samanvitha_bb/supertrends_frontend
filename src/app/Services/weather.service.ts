import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, filter, catchError, mergeMap } from 'rxjs/operators';
// import {RequestOptions, Request, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private _http: HttpClient) { }

  dailyForecast() {
    // return this._http.get('https://samples.openweathermap.org/data/2.5/history/city?q=Warren,OH&appid=b6907d289e10d714a6e88b30761fae22')
    return this._http.get('https://api.openweathermap.org/data/2.5/forecast?id=524901&APPID=a0ebc6cb812cd02138ac6a94c1d8f8cd')
    .pipe(
      map(result => result)
    );
  }
}
