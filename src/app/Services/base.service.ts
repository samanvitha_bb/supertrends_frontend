import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable} from 'rxjs';
import { map } from 'rxjs/operators';

/**** Config File ***/
import { APIConfig } from '../apiconfig';


@Injectable({
  providedIn: 'root'
})
export class BaseService {

  headers: any;
  currentUserToken: string = "";
  currentUser: any = localStorage.getItem('currentUser') ? JSON.parse(localStorage.getItem('currentUser')) : {};

  isDummyData: boolean = true;
  isLoader: boolean = false;
  

  /***********  Country/State/Cities Info  ************/
  AllCountriesList: any[] = [];
  AllStatesList: any[] = [];
  AllCitiesList: any[] = [];


  /***********  Profile Info  ************/
  currentUserProfile: any = {};
  myPredictionsData: any[] = [];
  selectedFilterPrediction: any = "All";

  /**************** Polls ****************/
  LatestActivePolls: any[] = [];
  LatestMyPolls: any[] = [];
  allLatestPolls: any[] = [];
  selectedPollComments: any[] = [];
  LatestMyPollPrediction: any = {};

  /**************** Questions ****************/
  allLatestQuestions: any[] = [];
  allLatestMyQuestions: any[] = [];
  selectedQuestion: any = {};
  selectedQuestionComments: any[] = [];


  /**************** Categories ****************/
  allLatestCategories: any[] = [];

  /**************** Posts ****************/
  LatestActivePosts: any[] = [];
  selectedPostComments: any[] = [];


  /**************** Users ****************/
  usersList: any = [];
  adminUserList: any = [];

  /**************** Dashboards ****************/
  LatestMyPredictions: any = [];


  /**************** Notification ****************/
  allActiveNotification: any = [];
  allNotification:any=[];


  constructor(public _Config: APIConfig, public _http: HttpClient) {

    //Refresh Profile Info
    this.refreshProfileInfo();

  }


  /**
   * Refresh Profile Info for Every 30 Minutes if user is Logedin
   * 30 * 60 * 1000 = 1800000
   */
  refreshProfileInfo() {

    //Refresh Profile Info
    setInterval(() => {
      if (this.currentUser.isLogedIn) {
        this.getProfileInfo();
      }
    }, 1800000);
  }

  /************** Common HTTP Request Methods *****************/

  /**
   * To Get Http Request for REST API
   * @param URL 
   * @param Options  
   */
  doGET(URL?: string, Options?: any) {
    return this._http.get(URL, Options);
  }

  /**
   * To Post Http Request for REST API
   * @param URL   // The Endpoint URL
   * @param Body    //Post Input Body
   * @param Options   //Headers or Other Optional Params
   */
  doPOST(URL?: string, Body?: any, Options?: any) {
    return this._http.post(URL, Body, Options);
  }

  /**
   * To Update Http Request for REST API
   * @param URL 
   * @param Body 
   * @param Options 
   */
  doPUT(URL?: string, Body?: any, Options?: any) {
    return this._http.put(URL, Body, Options);
  }


  /**
   * To Delete Http Request for REST API
   * @param URL 
   * @param Options 
   */
  doDelete(URL?: string, Options?: any) {
    return this._http.delete(URL, Options);
  }

  /**
   * If Patch Request for Http Restfull API
   * @param URL 
   * @param Body 
   * @param Options 
   */
  doPatch(URL?: string, Body?: any, Options?: any) {
    return this._http.patch(URL, Body, Options);
  }


  /**
   * To set Common API Header for all the HTTP Requsts based HTTP Methods
   * @param type    //If the type is Basic,Bearer With Master Key, Bearer with User Token 
   */
  commonHeaders(type: any) {

    if (type == "withUserToken") {
      let httpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + localStorage.getItem('userToken')
      });

      let headerOptions = {
        headers: httpHeaders
      };

      return headerOptions;
    }
    else if (type == "withBearerUserToken") {
      let httpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('userToken')
      });
      let headerOptions = {
        headers: httpHeaders
      };
      return headerOptions;

    }
    else if (type == "withBearer") {
      let httpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this._Config.MasterKey
      });
      let headerOptions = {
        headers: httpHeaders
      };

      return headerOptions;

    }
    else {
      let httpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
      });
      let headerOptions = {
        headers: httpHeaders
      };

      return headerOptions;
    }

  }


  /******************** Profile ************************/

  /**
   * To get Profile Info about the current user
   */
  async  getProfileInfo() {

    let requestURL = this._Config.API_Root + this._Config.getProfile;
    let headerOptions = this.commonHeaders("withBearerUserToken");

    await this.doGET(requestURL, headerOptions)
      .subscribe(
        (result: any) => {
          // this.currentUser = data;
          let data: any = result.data;
          this.setUserProfile(data);
        },
        (error: any) => {
          console.log("Rrror", error);
        }
      );
  }

  /**
  * To set the current user profile Info and role permission
  */
  setUserProfile(data: any) {
    data.isLogedIn = true;

    if (data.role == 'admin')
      data.isAdmin = true;
    else
      data.isAdmin = false;

    localStorage.setItem('currentUser', JSON.stringify(data));
    this.currentUser = data;
  }



  /********* Profile Page My Predictions Tab Filter Category Change event  ***********/
  selectMyPredictionCategory(selectedCategory: any) {
    this.isLoader = true;
    if (selectedCategory == "All") {
      this.selectedFilterPrediction = selectedCategory;
      this.myPredictionsData = this.allLatestPolls;
      this.isLoader = false;

    }
    else if (selectedCategory == "Pending") {

      this.selectedFilterPrediction = selectedCategory;

      let myPredictionArray: any = [];
      this.LatestMyPredictions.forEach(element => {
        let isItemFound = this.allLatestPolls.find(obj => obj.id !== element.question._id);
        if (isItemFound) {
          myPredictionArray.push(isItemFound);
        }

      });
      this.myPredictionsData = myPredictionArray;
      this.isLoader = false;

    }
    else if (selectedCategory == "Completed") {
      this.selectedFilterPrediction = selectedCategory;
      let myPredictionArray: any = [];
      this.LatestMyPredictions.forEach(element => {
        let isItemFound = this.allLatestPolls.find(obj => obj.id == element.question._id);
        if (isItemFound) {
          myPredictionArray.push(isItemFound);
        }

      });
      this.myPredictionsData = myPredictionArray;
      this.isLoader = false;


    }
  }


  /******************** Countries / State / City Lists ************************/

  /**
   * To get Countries
   */
  async  getCountries() {

    let requestURL = this._Config.GetAllCountries;
    // let headerOptions = this.commonHeaders("withBearerUserToken");

    await this.doGET(requestURL)
      .subscribe(
        (result: any) => {
          this.AllCountriesList = result.data;
        },
        (error: any) => {
          console.log("Rrror", error);
        }
      );
  }

  
  /**
   * To get All States
   */
  async  getStatesByCountryID(CountryID:any) {

    let requestURL = this._Config.GetStatesByCountryID;
    // let headerOptions = this.commonHeaders("withBearerUserToken");

    await this.doGET(requestURL)
    .pipe(map((response:any) => {
      debugger;
     return response.data.filter(item => item.country_id === CountryID)
    }))
      .subscribe(
        (result: any) => {
          debugger;
          this.AllStatesList = result;

        },
        (error: any) => {
          console.log("Rrror", error);
        }
      );
  }


  
  /**
   * To get all Cities
   */
  async  getCitiesByStateID(StateID:any) {

    let requestURL = this._Config.GetCitiesByStateID;
    // let headerOptions = this.commonHeaders("withBearerUserToken");

    await this.doGET(requestURL)
    .pipe(map((response:any) => {
     return response.data.filter(item => item.state_id === StateID)
    }))
      .subscribe(
        (result: any) => {
          this.AllCitiesList = result;
        },
        (error: any) => {
          console.log("Rrror", error);
        }
      );
  }

  /******************** Notification Functions ********************/

  /**
  * Get All Active Notification
  */
  async  getAllNotifications() {
    this.isLoader=true;
    let requestURL = this._Config.API_Root + this._Config.GetAllNotifications;
    let headerOptions = this.commonHeaders("withBearerUserToken");

    await this.doGET(requestURL, headerOptions)
      .subscribe(
        (response: any) => {
          let result: any = response.data;
          this.allNotification = result.rows;
          this.isLoader=false;
        },
        (error: any) => {
          console.log("Rrror", error);
        }
      );
  }


  /**
   * Get All  Notification
   */
  async  getActiveNotifications() {
    let currentDate: any = new Date();
    let timeStampFormatVal: any = Date.parse(currentDate);

    let requestURL = this._Config.API_Root + this._Config.GetLatestNotifications;
    requestURL = requestURL.replace("{TimestampData}", timeStampFormatVal);

    let headerOptions = this.commonHeaders("withBearerUserToken");

    await this.doGET(requestURL, headerOptions)
      .subscribe(
        (response: any) => {
          let result: any = response.data;
          this.allActiveNotification = result.rows;
        },
        (error: any) => {
          console.log("Rrror", error);
        }
      );
  }


  /******************** Category Functions ************************/

  /**
   * Get All Active Categories
   */
  async  getAllCategories() {
    this.isLoader = true;
    let requestURL = this._Config.API_Root + this._Config.getAllActiveCategoryList;
    let headerOptions = this.commonHeaders("withBearer");

    await this.doGET(requestURL, headerOptions)
      .subscribe(
        (response: any) => {
          let result: any = response.data;
      this.allLatestCategories = result.rows;
      this.isLoader = false;
        },
        (error: any) => {
          this.isLoader = false;
          console.log("Rrror", error);
        }
      );
  }


  /******************** Poll Functions ************************/

  /**
   *To Get all Active Polls List
   */
  async  getActivePolls() {

    let requestURL = this._Config.API_Root + this._Config.getAllActivePollList;
    let headerOptions = this.commonHeaders("withBearer");

    this.isLoader = true;

    await this.doGET(requestURL, headerOptions)
      .subscribe(
        (response: any) => {
          let result: any = response.data;

          this.LatestActivePolls = result;


          setTimeout(() => {
            this.isLoader = false;
          }, 500);
        },
        (error: any) => {
          console.log("Rrror", error);
          this.isLoader = false;

        }
      );
  }



  /**
   * To Get All Polls list
   */
  async  getAllPolls() {

    let requestURL = this._Config.API_Root + this._Config.getAllPollsList;
    let headerOptions = this.commonHeaders("withBearerUserToken");
    this.isLoader = true;

    await this.doGET(requestURL, headerOptions)
      .subscribe(
        (response: any) => {
          let result: any = response.data;
          this.isLoader = false;
          this.allLatestPolls = result.rows;
        },
        (error: any) => {
          console.log("Rrror", error);
          this.isLoader = false;
        }
      );
  }



  /**
   *To Get all My Polls List
   */
  async  getMyPolls() {

    let requestURL = this._Config.API_Root + this._Config.getAllMyPollList;
    let headerOptions = this.commonHeaders("withBearerUserToken");

    this.isLoader = true;

    await this.doGET(requestURL, headerOptions)
      .subscribe(
        (response: any) => {
          
          let result: any = response.data;
          this.LatestMyPolls = result.rows;

          setTimeout(() => {
            this.isLoader = false;
          }, 500);
        },
        (error: any) => {
          console.log("Rrror", error);
          this.isLoader = false;

        }
      );
  }


  /**
  *To Get all Active Polls Comments List
  */
  async  getPollsCommentsByID(PollID: any) {

    let requestURL = this._Config.API_Root + this._Config.GetPollCommentById;
    requestURL = requestURL.replace("{PollID}", PollID);

    let headerOptions = this.commonHeaders("withBearerUserToken");


    await this.doGET(requestURL, headerOptions)
      .subscribe(
        (response: any) => {
          let result: any = response.data;
          this.selectedPollComments = result.rows;

        },
        (error: any) => {
          console.log("Rrror", error);
          this.isLoader = false;

        }
      );
  }


  /**
  *To Get all Active Polls List
  */
  async  GetMyPollPredictionById(PollID) {

    let requestURL = this._Config.API_Root + this._Config.GetMyPollPredictionById;
    requestURL = requestURL.replace("{PollID}", PollID);

    let headerOptions = this.commonHeaders("withBearerUserToken");

    this.isLoader = true;

    await this.doGET(requestURL, headerOptions)
      .subscribe(
        (response: any) => {
          let result: any = response.data;

          this.LatestMyPollPrediction = result;

          setTimeout(() => {
            this.isLoader = false;
          }, 500);
        },
        (error: any) => {
          console.log("Rrror", error);
          this.isLoader = false;

        }
      );
  }


  /******************** Questions Functions ************************/

  /**
   * To Get All Active Questions
   */
  async  getAllQuestions() {

    // let requestURL = this._Config.API_Root + this._Config.getAllQuestion;
    let requestURL = this._Config.API_Root + this._Config.getActiveQuestionList;
    let headerOptions = this.commonHeaders("withBearer");

    this.isLoader = true;

    await this.doGET(requestURL, headerOptions)
      .subscribe(
        (response: any) => {

          let result: any = response.data;

          this.allLatestQuestions = result;
          setTimeout(() => {
            this.isLoader = false;
          }, 500);


        },
        (error: any) => {
          console.log("Rrror", error);
          this.isLoader = false;

        }
      );
  }

  /**
   * To get All My questions If the Current User/Admin is Loged in
   */
  async  getAllMyQuestions() {

    let requestURL = this._Config.API_Root + this._Config.getAllMyQuestionList;
    let headerOptions = this.commonHeaders("withBearerUserToken");

    await this.doGET(requestURL, headerOptions)
      .subscribe(
        (response: any) => {
          let result: any = response.data;
          this.isLoader = false;

          let resultArray = result.rows;

          this.allLatestMyQuestions = resultArray;


        },
        (error: any) => {
          console.log("Rrror", error);
        }
      );
  }



  /**
   * To Get Selected Question By Question ID
   * @param QuesionID 
   * //api/v1/question/:QID
   */
  async  ViewQuestionByID(QuesionID: any) {

    let requestURL = this._Config.API_Root + this._Config.getQuestionById;
    requestURL = requestURL.replace("{QuestionID}", QuesionID);

    let headerOptions = this.commonHeaders("withBearerUserToken");

    await this.doGET(requestURL, headerOptions)
      .subscribe(
        (result: any) => {
          this.selectedQuestion = result;
        },
        (error: any) => {
          console.log("Rrror", error);
        }
      );
  }


  /**
 *To Get all Active Question Comments List
 */
  async  getQuestionsCommentsByID(QuestionID: any) {

    let requestURL = this._Config.API_Root + this._Config.GetQuestionCommentById;
    requestURL = requestURL.replace("{QuestionID}", QuestionID);

    let headerOptions = this.commonHeaders("withBearerUserToken");


    await this.doGET(requestURL, headerOptions)
      .subscribe(
        (response: any) => {
          let result: any = response.data;
          this.selectedQuestionComments = result.rows;

        },
        (error: any) => {
          console.log("Rrror", error);
          this.isLoader = false;

        }
      );
  }


  /******************** Posts Functions ************************/

  /**
   * To Get all Active Posts
   */
  async  getActivePosts() {
    this.isLoader = true;
    let requestURL = this._Config.API_Root + this._Config.getActivePostList;
    let headerOptions = this.commonHeaders("withBearer");

    await this.doGET(requestURL, headerOptions)
      .subscribe(
        (response: any) => {
          let result: any = response.data;
          let resultArray = [];

          this.LatestActivePosts = result;
          // this.LatestActivePostst = result.rows;

          setTimeout(() => {
            this.isLoader = false;
          }, 500);

        },
        (error: any) => {
          console.log("Rrror", error);
          this.isLoader = false;
        }
      );
  }


  /**
  *To Get all Active Polls Comments List
  */
  async  getPostsCommentsByID(PostID: any) {

    let requestURL = this._Config.API_Root + this._Config.GetPostCommentById;
    requestURL = requestURL.replace("{PostID}", PostID);

    let headerOptions = this.commonHeaders("withBearerUserToken");


    await this.doGET(requestURL, headerOptions)
      .subscribe(
        (response: any) => {
          let result: any = response.data;
          this.selectedPostComments = result.rows;

        },
        (error: any) => {
          console.log("Rrror", error);
          this.isLoader = false;

        }
      );
  }


  /******************** Users Functions ************************/

  /**
   * To Get all users
   */
  async  getUsersList() {
    this.isLoader = true;
    let requestURL = this._Config.API_Root + this._Config.getAllUserList;
    let headerOptions = this.commonHeaders("withBearerUserToken");

    await this.doGET(requestURL, headerOptions)
      .subscribe(
        (response: any) => {
          console.log('response--------------------', response);
          this.usersList = response.data.rows;
          this.adminUserList = this.usersList.filter(user => user.role == 'admin');


        },
        (error: any) => {
          console.log("Rrror", error);
          this.isLoader = false;
        }
      );
  }





  /******************** Dashboard Functions ************************/


  /**
   *To Get all My Polls List
   */
  async  getMyPredictions() {

    let requestURL = this._Config.API_Root + this._Config.get_predictions;
    let headerOptions = this.commonHeaders("withBearerUserToken");

    this.isLoader = true;

    await this.doGET(requestURL, headerOptions)
      .subscribe(
        (response: any) => {
          
          let result: any = response.data;
          this.LatestMyPredictions = result.rows;

          setTimeout(() => {
            this.isLoader = false;
          }, 500);
        },
        (error: any) => {
          console.log("Rrror", error);
          this.isLoader = false;

        }
      );
  }



  /**
  * Get All Active Notification
  */
 async  getAllCountries() {
  this.isLoader=true;
  let requestURL = "http://localhost/locations-api/api/country";
  let headerOptions = this.commonHeaders("withBearerUserToken");

  await this.doGET(requestURL, headerOptions)
    .subscribe(
      (response: any) => {
        
        let result: any = response.data;
        this.allNotification = result.rows;
        this.isLoader=false;
      },
      (error: any) => {
        console.log("Rrror", error);
      }
    );
}



}
