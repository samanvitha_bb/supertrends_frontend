import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'pre-loader',
  templateUrl: './pre-loader.component.html',
  styleUrls: ['./pre-loader.component.scss']
})
export class PreLoaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
