import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';
import { Routes, RouterModule, Router, ActivatedRoute } from "@angular/router";


/**** Config File ***/
import { APIConfig } from '../apiconfig';

import { BaseService } from '../Services/base.service';
import { PollService } from '../Services/poll.service';
import { UtilityService } from '../utility.service';

import { async } from '@angular/core/testing';


@Component({
  selector: 'app-ama',
  templateUrl: './ama.component.html',
  styleUrls: ['./ama.component.scss']
})
export class AmaComponent {
    [x: string]: any;

    constructor(public _Config: APIConfig, private _pollService: PollService, private _baseService: BaseService,
        private router: Router, private route: ActivatedRoute, private _utilityService: UtilityService) { 
    }

    ngOnInit() {
        this.uploader = new FileUploader({ method: "PUT", itemAlias: 'picture', authToken: 'Bearer ' + localStorage.getItem('userToken') });
        this.getActiveAMAsList();
        this.InitializeForm();
        this._baseService.getUsersList();
        if (this._baseService.currentUser.isLogedIn) {
            /********** File Upload Configurations **********/
            this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
            this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      
              let responseData: any = JSON.parse(response);
              if (responseData.statusCode == 200) {
                this.tempPreview = "";
              }
              else {
                this._utilityService.showError('Error!', responseData.message);
              }
      
            };
      
        }
    }

    assignAndReassignLabel: string;
    myAssignedAMAList: any;
    QuestionForm: FormGroup;
    createAMAFORM: FormGroup;
    isFormShow: boolean = false;
    isDummyData: boolean = true;
    isEdit: boolean = false;
    discussionData: any;
    assignAdminForAnswer: boolean = false;
    showRecentAMAsDeatailsToggle: boolean = false;
    selectedType = 'newly';
    defaultImage = 'assets/img/avator.png';
    public uploader: FileUploader;
    tempPreview: any = "";
    isListPage: boolean = true;
    sortBYDropDownList = [{
        name: 'Newest',
        sortingKey: 'true',
    }, {
        name: 'Oldest',
        sortingKey: 'false',
    }];
    selectedCategory: any = { id: "", name: "All" };
    detailsPageLabel: any = 'Starting Soon';
    isViewMoreToggle: boolean = true;
    selectedUser: any = {
        id: 0,
        firstName: '',
        lastName: '',
        owner: {
            firstName: '',
            lastName: ''
        }
    };
    showDetails: boolean = false;
    SearchKeyword:any="";
    isOrderby:boolean= false;
    formButtonsDisplayName = "SCHEDULE AMA";
    isAMAQuestionCreate: boolean = false;
    formsHeading: any;
    startDate: any;
    startTime: any;
    endTime: any;
    endDate: any;
    minDate = new Date();

    getActiveAMAsList() {
        this._baseService.isLoader = true;
        let requestURL = this._Config.API_Root + this._Config.activeAMAList;
        let headerOptions = this._baseService.commonHeaders("withBearer");
        this._baseService.doGET(requestURL, headerOptions)
          .subscribe(
            (response: any) => {
                this.showHideQuestionForm(false);
                this.discussionData = response.data;
                this.discussionData.activeAMAList = this.discussionData.newly;
                this.discussionData.passedAMAs = this.discussionData.activeAMAList.filter(a => new Date(a.startDate) < this.minDate);
                this.discussionData.newly = this.discussionData.activeAMAList.filter(a => new Date(a.startDate) > this.minDate);
                console.log('this.discussionData', this.discussionData);
                if(this.isOrderby) {
                    this.showRecentAMAsDeatails = response.data.newly[3];
                } else {
                    this.showRecentAMAsDeatails = response.data.newly[0];
                }
                this._baseService.isLoader = false;
            },
            (error: any) => {
              console.log("Rrror", error);
              this._baseService.isLoader = false;
            }
          );
    }

    //Category Change event
    selectCategory(selectedCategory: any) {
        if (selectedCategory == "All") {
          this.selectedCategory = { id: "", name: "All" };
        }
        else{
          this.selectedCategory = selectedCategory;
        }
    }

    showAMAsDetails(selectedAMAsList: any, selectListLabel: any) {
        console.log('selectedAMAsList', selectedAMAsList, selectedAMAsList.created_by._id);
        this.detailsPageLabel = selectListLabel;
        if(selectListLabel == 'Starting Soon') {
            this.selectedType = 'newly';
        } else if(selectListLabel == 'Passed AMAs') {
            this.selectedType = 'passedAMAs';
        } else {
            this.selectedType = 'popular';
        }
        this.showRecentAMAsDeatailsToggle = true;
        this.showRecentAMAsDeatails = selectedAMAsList;
    }

    selectOrderBy(sortingKey: any) {
        this.showRecentAMAsDeatailsToggle = false;
        this.isOrderby = sortingKey;
    }

    navidateToAMAsDetailsPage() {
        //this.router.navigate(['/ama/detail/' + this.showRecentAMAsDeatails.id]);
        this.selectedItem = this.showRecentAMAsDeatails;
        this.isListPage = !this.isListPage;
    }

    handleBackChange(event: any) {
        this.isListPage = event;
    }

      //Show or Hide Form
    showHideQuestionForm(type: boolean, typeOfAction?: string) {
        if(typeOfAction == 'createAMAQuestion')
            this.isAMAQuestionCreate = true;
        else
            this.isAMAQuestionCreate = false;

        this.isFormShow = type;
        this.formButtonsLabel();
    }

    formButtonsLabel() {

        if(this.isEdit && this._baseService.currentUser.isAdmin) {
            this.formButtonsDisplayName = 'Update AMA';
            this.formsHeading = 'Update a AMA'; 
        } else if(this.isAMAQuestionCreate) {
            this.formButtonsDisplayName = 'Create Question';
            this.formsHeading = 'Ask a Question';
        } else {
            this.formButtonsDisplayName = 'SCHEDULE AMA';
            this.formsHeading = 'SCHEDULE AMA';
        }
    }

      //Initialize Form
    InitializeForm() {
        this.createAMAFORM = new FormGroup({
            hostName: new FormControl('', Validators.required),
            hostNameDescriptor: new FormControl('', Validators.required),
            personalDescriptor: new FormControl('', Validators.required),
            tags: new FormControl('', Validators.required),
            title: new FormControl('', Validators.required),
            description: new FormControl('', Validators.required),
        });
    }

    resetAMAPage() {
        this.getActiveAMAsList();
        this.InitializeForm();
        this.selectCategory('All');
        this.showHideQuestionForm(false);
        this.isEdit = false;
    }

    onValueChange(value: any, typeOfDate: any) {
        console.log('value', value, 'typeOfDate', typeOfDate);
        if(typeOfDate == 'startDate')
            this.startDate = value;
            else
            this.endDate = value;
    }

    createAMAList() {
        console.log('this.selectedCategory.id----------', this.selectedCategory.id, this.startDate, this.startTime, this.endTime)
        if (!this.tempPreview) {
            this._utilityService.showError('Error!', "Please add AMA profile image");
        } else if(! this.selectedCategory.id) {
            this._utilityService.showError('Error!', "Please Select Category");
            
        }  else if(! this.startDate) {
            this._utilityService.showError('Error!', "Please Select START DATE");
            
        } else if(! this.startTime) {
            this._utilityService.showError('Error!', "Please Select START TIME");
            
        }  else if(! this.endTime) {
            this._utilityService.showError('Error!', "Please Select END TIME");
            
        } else {
            let startTimeStampFormat = Date.parse(this.startDate);
            let TagsList = [];
            let personalDescriptor = [];
            personalDescriptor = this.createAMAFORM.value.personalDescriptor.split(",");
            TagsList = this.createAMAFORM.value.tags.split(",");
            console.log('TagsList,' , TagsList);
            let PostBody = {
                hostname: this.createAMAFORM.value.hostName,
                personalDescriptor: personalDescriptor,
                category: this.selectedCategory.id,
                title: this.createAMAFORM.value.title,
                description: this.createAMAFORM.value.description,
                startDate: Number(startTimeStampFormat),
                tags: TagsList,
            };
    
            let requestURL = this._Config.API_Root + this._Config.CreateAMA;
    
            console.log('requestURL---------create Question', requestURL, PostBody);
    
            let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
            this._baseService.doPOST(requestURL, PostBody, headerOptions)
            .subscribe(
                (response: any) => {
                    let responseData: any = response;
                    if (responseData.statusCode == 200) {
                        this.uploadProfileImage(responseData.data.id);
                        this.resetAMAPage();
                        this.formButtonsLabel();
                        this.startDate = null;
                        this.endDate = null;
                        this._utilityService.showSuccess("Success!", "AMA's is created successfully.");
                        this.showRecentAMAsDeatailsToggle = false;
                    }
                    else {
                        this._utilityService.showError('Error!', responseData.message);
                    }
                },
                (error: any) => {
                console.log("Rrror", error);
                }
            );
        }
    }

    createAMA() {
        this.InitializeForm();
        this.selectCategory('All');
        this.showHideQuestionForm(true);
        this.isEdit = false;
        this.formButtonsLabel();
    }

    onFileChange(event) {
        let self = this;
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
          var file: File = event.target.files[0];
          var myReader: FileReader = new FileReader();
    
          myReader.onloadend = (e) => {
            self.tempPreview = myReader.result;
          }
          myReader.readAsDataURL(file);
    
    
        }
      }
        /**
         * To Initialize and Upload Picture
         */
        uploadProfileImage(amaID: any) {
            console.log('amaID--------', amaID);
            let requestURL = this._Config.API_Root + this._Config.UpdateAMAPicture;
            requestURL = requestURL.replace("{AMA_ID}", amaID);

            /********** File Upload Configurations **********/
            this.apiURL = requestURL;
            // this.uploader = new FileUploader({ method: "PUT", url: this.apiURL, itemAlias: 'picture', authToken: 'Bearer ' + localStorage.getItem('userToken') });

            // Update uploader URL
            this.uploader.setOptions({ method: "PUT", url: this.apiURL, itemAlias: 'picture', authToken: 'Bearer ' + localStorage.getItem('userToken') });

            // Clear the item queue (somehow they will upload to the old URL)
            // this.uploader.clearQueue();

            this.uploader.uploadAll();
        }

        viewALLAMAs() {
            this.isViewMoreToggle = ! this.isViewMoreToggle;
        }
}
