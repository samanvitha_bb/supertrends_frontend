import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Routes, RouterModule, Router, ActivatedRoute } from "@angular/router";
import { FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';


/**** Config File ***/
import { APIConfig } from '../apiconfig';

/**** Base Service File ***/
import { BaseService } from '../Services/base.service';
import { UtilityService } from '../utility.service';


@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent {

  FormObject: any = {};

  QuestionForm: FormGroup;
  isFormShow: boolean = false;
  selectedCategory: any = { id: "", name: "All" };

  selectedQuestion: any = {};
  isEdit: boolean = false;
  showDetails: boolean = false;
  selectedItem: any = {};
  SearchKeyword: any = "";

  isOrderby: boolean = true;


  selectedFile: File;
  allFilesList: any = [];
  public uploader: FileUploader;
  public apiURL: any = "";

  tempPreview: any = "";

  defaultImage = 'assets/img/touch.jpg';
  offset = 0;

  constructor(public _Config: APIConfig, private _baseService: BaseService,
    private _utilityService: UtilityService,
    private router: Router, private route: ActivatedRoute) {

  }


  ngOnInit() {

    this._baseService.getAllQuestions();

    if (this._baseService.currentUser.isLogedIn) {
      this._baseService.getAllMyQuestions();
    }

    // this.InitializeForm();

    this.uploader = new FileUploader({ method: "PUT", itemAlias: 'picture', authToken: 'Bearer ' + localStorage.getItem('userToken') });

    if (this._baseService.currentUser.isLogedIn) {

      /********** File Upload Configurations **********/
      this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
      this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {

        let responseData: any = JSON.parse(response);
        if (responseData.statusCode == 200) {
          this._baseService.getAllQuestions();
          this._utilityService.showSuccess('Success!', 'Question Created successfully.');
          this.showHideQuestionForm(false);

        }
        else {
          this._utilityService.showError('Error!', responseData.message);
        }


      };

    }


  }


  /*********  Show or Hide Form  ***********/
  showHideQuestionForm(type: boolean) {
    this.isFormShow = type;

    if (!type) {
      this.isEdit = false;
    }

    if (!this.isEdit) {
      this.FormObject={};
    }

  }

  


  /*********  Category Change event  ***********/
  selectCategory(selectedCategory: any) {
    if (selectedCategory == "All") {
      this.selectedCategory = { id: "", name: "All" };
    }
    else{
      this.selectedCategory = selectedCategory;
    }
  }



  /*********  On Submit To Create or Update Questions ***********/
  onQuestionFormSubmit() {

    if (!this.tempPreview) {
      this._utilityService.showError('Error!', "Please add question image");
    }
    else{
      let PostBody = {
        title: this.FormObject.Title,
        description: this.FormObject.Description,
        category: this.FormObject.Category,
      };
  
      /******* For Update If isEdit=true *****/
      if (this.isEdit) {
        let requestURL = this._Config.API_Root + this._Config.UpdateQuestion;
        requestURL = requestURL.replace("{QuestionID}", this.selectedQuestion.id);
  
        let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
        this._baseService.doPUT(requestURL, PostBody, headerOptions)
          .subscribe(
            (response: any) => {
  
              let responseData: any = response;
              if (responseData.statusCode == 200) {
  
                this.uploadImage(responseData.data.id);
  
              }
              else {
                this._utilityService.showError('Error!', responseData.message);
              }
  
  
  
  
            },
            (error: any) => {
              console.log("Rrror", error);
            }
          );
  
      }
      else {
        let requestURL = this._Config.API_Root + this._Config.CreateQuestion;
        let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
        this._baseService.doPOST(requestURL, PostBody, headerOptions)
          .subscribe(
            (response: any) => {
  
              let responseData: any = response;
              if (responseData.statusCode == 200) {
  
                this.uploadImage(responseData.data.id);
  
              }
              else {
                this._utilityService.showError('Error!', responseData.message);
              }
  
  
  
            },
            (error: any) => {
              console.log("Rrror", error);
            }
          );
      }
    }
    


  }


  /*********  Set Edit Question Items ***********/
  editQuestion(selectedQuestion: any) {

    this.selectedQuestion = selectedQuestion;

    this.QuestionForm = new FormGroup({
      Title: new FormControl(this.selectedQuestion.title, Validators.required),
      Description: new FormControl(this.selectedQuestion.description, Validators.required),
    });

    this.selectCategory(this.selectedQuestion.question_type);
    this.isEdit = true;
    this.showHideQuestionForm(true);


  }





  /**
   * To Initialize and Upload Picture
   */
  uploadImage(QuestionID: any) {

    let requestURL = this._Config.API_Root + this._Config.UpdateQuestionPictureById;
    requestURL = requestURL.replace("{QuestionID}", QuestionID);

    /********** File Upload Configurations **********/
    this.apiURL = requestURL;
    // this.uploader = new FileUploader({ method: "PUT", url: this.apiURL, itemAlias: 'picture', authToken: 'Bearer ' + localStorage.getItem('userToken') });

    // Update uploader URL
    this.uploader.setOptions({ method: "PUT", url: this.apiURL, itemAlias: 'picture', authToken: 'Bearer ' + localStorage.getItem('userToken') });

    this.uploader.uploadAll();
  }

  
  onFileChange(event) {
    let self = this;
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      var file: File = event.target.files[0];
      var myReader: FileReader = new FileReader();

      myReader.onloadend = (e) => {
        self.tempPreview = myReader.result;
      }
      myReader.readAsDataURL(file);


    }
  }


  /*********  Delete Question By ID ***********/
  DeleteQuestionsByID(QuestionById: any) {
    let requestURL = this._Config.API_Root + this._Config.DeleteQuestion;
    requestURL = requestURL.replace("{QuestionID}", QuestionById);

    let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
    this._baseService.doDelete(requestURL, headerOptions)
      .subscribe(
        (data: any) => {
          this._baseService.getAllQuestions();
        },
        (error: any) => {
          console.log("Rrror", error);
        }
      );
  }


  /*********  View Question By ID ***********/
  ViewItem(selectedQuestion: any) {
    this.showDetails = true;
    this.selectedItem = selectedQuestion;
    window.scroll(0, 0);
  }


  /*********  To Handle Detail page Back Button Event ***********/
  handleBackChange(event: any) {
    this.showDetails = !event;
    // this.InitializeForm();
    this._baseService.getAllQuestions();

  }

  /*********  To Sort by Ascending or Descending Order ***********/
  selectOrderBy(type: boolean) {
    this.isOrderby = type;
  }



}
