import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Routes, RouterModule, Router, ActivatedRoute } from "@angular/router";


/**** Config File ***/
import { APIConfig } from '../../apiconfig';

import { BaseService } from '../../Services/base.service';
import { UtilityService } from '../../utility.service';


@Component({
  selector: 'app-question-detail',
  templateUrl: './question-detail.component.html',
  styleUrls: ['./question-detail.component.scss']
})
export class QuestionDetailComponent implements OnInit {

  @Input() selectedItem: any;
  @Input() type: any;
  @Output() isBack = new EventEmitter();

  discussionData: any[] = [
    {
      'title': 'When will Tesla make its entry into the Indian market?',
      'topic': 'trending',
      'category': 'automobile /  cars electrical cars',
      'user': 'Bob Business',
      'userimg': '',
      'time': '5  hours ago',
      'msgcount': '32',
      'count': '500',
      'cardbg': ''
    },
  ];

  selectedItemType: any = "";
  selectedItemInfo: any = {};
  postCommentInput: any = "";
  isOrderby: boolean = true;

  defaultImage = 'assets/img/touch.jpg';
  offset = 0;

  ReplyComment: any = "";
  selectedCommentReplies: any = [];

  constructor(public _Config: APIConfig, 
    private _utilityService: UtilityService, private _baseService: BaseService,
    private router: Router, private route: ActivatedRoute) {

    let QuestionID = localStorage.getItem("selectedQuestion");
    if (QuestionID)
      this._baseService.ViewQuestionByID(QuestionID);
    else
      this.router.navigate(['/questions']);

  }


  ngOnInit() {
    debugger;

    if(this.type=="Question")
    this.selectedItemType = "Q&A";

    this.selectedItemInfo = this.selectedItem;

    if (this._baseService.currentUser.isLogedIn) {
      this.ViewDetailItem();
      this._baseService.getQuestionsCommentsByID(this.selectedItem.id);

      this._baseService.selectedQuestionComments.forEach(element => {
        element.isReply = false;
      });
    }

  }


  goBack() {
    this.isBack.emit(true);
  }


  

  /**
   * To make Order by Ascending or Descending
   * @param type 
   */
  selectOrderBy(type: boolean) {
    this.isOrderby = type;
  }

  ViewDetailItem() {
    if (this._baseService.currentUser.isLogedIn) {

      let requestURL = this._Config.API_Root + this._Config.getQuestionById;
      requestURL = requestURL.replace("{QuestionID}", this.selectedItem.id);

      let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
      this._baseService.doGET(requestURL, headerOptions)
        .subscribe(
          (response: any) => {

          },
          (error: any) => {
            console.log("Rrror", error);
          }
        );

    }
  }

  likeItem() {
    if (this._baseService.currentUser.isLogedIn) {

      let requestURL = this._Config.API_Root + this._Config.LikeQuestionById;
      requestURL = requestURL.replace("{QuestionID}", this.selectedItem.id);

      let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
      this._baseService.doPUT(requestURL, {}, headerOptions)
        .subscribe(
          (response: any) => {
            let responseData: any = response;
            if (responseData.statusCode == 200) {

              let responseObj = responseData.data;
              if (responseObj) {

                if (responseObj.like_count == this.selectedItem.like_count) {
                  this._utilityService.showWarning('Warning!', "You have already liked this question!");
                }
                else {
                  this.selectedItem.like_count = responseObj.like_count;
                  this._utilityService.showSuccess('Success!', "Question Liked Successfully.");
                }

              }



            }
            else {
              this._utilityService.showError('Error!', responseData.message);
            }
          },
          (error: any) => {
            console.log("Rrror", error);
          }
        );

    }
  }


  postComment() {

    if (this._baseService.currentUser.isLogedIn) {

      let PostBody = {
        comment: this.postCommentInput,
      };

      this._baseService.isLoader = true;

      let requestURL = this._Config.API_Root + this._Config.UpdateQuestionCommentById;
      requestURL = requestURL.replace("{QuestionID}", this.selectedItem.id);

      let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
      this._baseService.doPUT(requestURL, PostBody, headerOptions)
        .subscribe(
          (response: any) => {

            let responseData: any = response;
            if (responseData.statusCode == 200) {

              this._baseService.isLoader = false;

              let responseObj = responseData.data;
              if (responseObj)
                this._baseService.getQuestionsCommentsByID(responseObj.id);

              this.postCommentInput = "";
              this._utilityService.showSuccess('Success!', "Comment Posted Successfully.");

              this.selectedItemInfo.comment_count = this.selectedItemInfo.comment_count + 1;

            }
            else {
              this._utilityService.showError('Error!', responseData.message);
            }

          },
          (error: any) => {
            console.log("Rrror", error);
          }
        );

    }
    else {
      this._utilityService.showWarning("", "Please Login and post your comment!");
    }


  }


  showReply(selectedItem) {
    this._baseService.selectedQuestionComments.forEach(element => {
      element.isReply = false;
    });

    let itemIndex = this._baseService.selectedQuestionComments.findIndex(obj => obj.id == selectedItem.id);
    this._baseService.selectedQuestionComments[itemIndex].isReply = true;
  }


  postReplyComment(selectedItem) {

    if (this._baseService.currentUser.isLogedIn) {

      let PostBody = {
        comment: this.ReplyComment,
      };

      this._baseService.isLoader = true;

      let requestURL = this._Config.API_Root + this._Config.UpdateQuestionCommentReplyByCommentId;
      requestURL = requestURL.replace("{QuestionID}", this.selectedItem.id);
      requestURL = requestURL.replace("{CommentID}", selectedItem.id);

      let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
      this._baseService.doPUT(requestURL, PostBody, headerOptions)
        .subscribe(
          (response: any) => {

            let responseData: any = response;
            if (responseData.statusCode == 200) {

              this._baseService.isLoader = false;

              let responseObj = responseData.data;
              if (responseObj)
                this._baseService.getQuestionsCommentsByID(this.selectedItem.id);

              this.postCommentInput = "";
              this._utilityService.showSuccess('Success!', "Comment Posted Successfully.");

              this.selectedItemInfo.comment_count = this.selectedItemInfo.comment_count + 1;

            }
            else {
              this._utilityService.showError('Error!', responseData.message);
            }

          },
          (error: any) => {
            console.log("Rrror", error);
          }
        );

    }
    else {
      this._utilityService.showWarning("", "Please Login and post your comment!");
    }


  }


  likeCommentItem(SelectedComment: any, type: any) {
    if (this._baseService.currentUser.isLogedIn) {

      let requestURL = this._Config.API_Root + this._Config.UpdateQuestionCommentLikeByCommentId;
      requestURL = requestURL.replace("{QuestionID}", this.selectedItem.id);
      requestURL = requestURL.replace("{CommentID}", SelectedComment.id);


      let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
      this._baseService.doPUT(requestURL, {}, headerOptions)
        .subscribe(
          (response: any) => {
            let responseData: any = response;
            if (responseData.statusCode == 200) {

              let responseObj = responseData.data;
              if (responseObj) {

                if (responseObj.like_count == SelectedComment.like_count) {
                  this._utilityService.showWarning('Warning!', "You have already liked this comment!");
                }
                else {

                  if (type == 1) {
                    let itemIndex = this._baseService.selectedQuestionComments.findIndex(obj => obj.id == SelectedComment.id);
                    this._baseService.selectedQuestionComments[itemIndex].like_count = responseObj.like_count;

                  }
                  // else {
                  //   let itemIndex = this._baseService.selectedQuestionComments.ReplyComments.findIndex(obj => obj.id == SelectedComment.id);
                  //   this._baseService.selectedQuestionComments[itemIndex].like_count = responseObj.like_count;

                  // }
                  this._utilityService.showSuccess('Success!', "Comment Liked Successfully.");
                }

              }

            }
            else {
              this._utilityService.showError('Error!', responseData.message);
            }
          },
          (error: any) => {
            console.log("Rrror", error);
          }
        );

    }
  }

  /**
 *To Get all Active Polls Comments List
 */
  async  showAllReplies(selectedItem: any) {

    let requestURL = this._Config.API_Root + this._Config.GetQuestionCommentReplyByCommentId;
    requestURL = requestURL.replace("{QuestionID}", this.selectedItem.id);
    requestURL = requestURL.replace("{CommentID}", selectedItem.id);

    let headerOptions = this._baseService.commonHeaders("withBearerUserToken");

    this._baseService.isLoader = true;


    await this._baseService.doGET(requestURL, headerOptions)
      .subscribe(
        (response: any) => {
          let result: any = response.data;
          debugger;
          this.selectedCommentReplies = result.rows;

          let itemIndex = this._baseService.selectedQuestionComments.findIndex(obj => obj.id == selectedItem.id);
          this._baseService.selectedQuestionComments[itemIndex].ReplyComments = result.rows;

          setTimeout(() => {
            this._baseService.isLoader = false;
          }, 500);


        },
        (error: any) => {
          console.log("Rrror", error);
          this._baseService.isLoader = false;

        }
      );
  }





}
