import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter, TemplateRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Routes, RouterModule, Router, ActivatedRoute } from "@angular/router";
import { FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';


/**** Config File ***/
import { APIConfig } from '../../apiconfig';

/**** Base Service File ***/
import { BaseService } from '../../Services/base.service';
import { UtilityService } from '../../utility.service';

@Component({
  selector: 'app-create-poll',
  templateUrl: './create-poll.component.html',
  styleUrls: ['./create-poll.component.scss']
})
export class CreatePollComponent implements OnInit {

  @Input() selectedItem: any;
  @Input() type: any;
  @Output() isBack = new EventEmitter();

  polls = [];
  itemCount: number;
  pollText: string = '';
  btnText: string = 'Add item';

  pollFormObject: any = {
    PredictionType: "Month/Year"
  };
  selectedCategory: string = "All";

  tags: any;


  selectedFile: File;
  allFilesList: any = [];
  uploader: FileUploader;
  apiURL: any = "";
  tempPreview: any = "";

  defaultImage = 'assets/img/touch.jpg';


  modalRef: BsModalRef;
  message: string;

  createdPoll: any = {};

  showVote: boolean = false;
  voteMonth: any = {};
  monthsArray: any[] = [];

  yearsArr: any = [];
  yearSelect: any = "YYYY";
  monthSelect: any = "MM";

  Days: any = [];

  constructor(public _Config: APIConfig, private _baseService: BaseService,
    private _utilityService: UtilityService,
    private modalService: BsModalService,
    private router: Router, private route: ActivatedRoute) {



  }

  ngOnInit() {

    this.setMonthandYears();


    this.uploader = new FileUploader({ method: "PUT", itemAlias: 'picture', authToken: 'Bearer ' + localStorage.getItem('userToken') });

    if (this._baseService.currentUser.isLogedIn) {

      /********** File Upload Configurations **********/
      this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
      this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {

        let responseData: any = JSON.parse(response);
        if (responseData.statusCode == 200) {
          this.tempPreview = "";
          this._baseService.GetMyPollPredictionById(responseData.data.id);
          // this.cancelCreatePoll();
          // this._utilityService.showSuccess('Success!', 'Poll Created successfully.');
          this.showVote = true;
          this._baseService.isLoader = false;
          this.createdPoll = responseData.data;

        }
        else {
          this._utilityService.showError('Error!', responseData.message);
          this._baseService.isLoader = false;

        }

      };

    }


  }


  setMonthandYears() {

    /**************** To List All Months**********************/

    // var monthLarge = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var monthSmall = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

    var d = new Date();
    var Year = d.getFullYear();

    for (let i = 0; i <= monthSmall.length; i++) {

      let month = {
        label: monthSmall[i],
        month: i,
        year: Year
      };
      this.monthsArray.push(month);

      let item = "";
      if (i < 10)
        item = '0' + i;
      else
        item = '' + i;

      let monthItem = {
        label: item,
        value: i + 1
      };

      if (i != 0)
        this.Days.push(monthItem);



    }



    /**************** To get Full Years**********************/
    let year = new Date().getFullYear();
    let range = [];

    for (var i = 0; i < 31; i++) {
      range.push({
        label: year + i,
        value: year + i
      });

    }
    this.yearsArr = range;
    // this.yearSelect=this.yearsArr[0].value;

  }

  /**
   *Set Category by Change event
   * @param selectedCategory 
   */
  selectCategory(selectedCategory: any) {
    this.selectedCategory = selectedCategory;
  }

  onMonthSelectionChange(selecetedItem) {
    this.voteMonth = selecetedItem;
  }

  PredictionTypeChange(Type: any) {
    this.pollFormObject.PredictionType = Type;
    this.monthSelect = "MM";
    this.yearSelect = "YYYY";
  }


  /**
   * On Submit To create a Poll 
   */
  onpollFormSubmit() {

    if (!this.tempPreview) {
      this._utilityService.showError('Error!', "Please add poll image");
    }
    else {

      let TagsList = [];
      if (this.pollFormObject.Tags)
        TagsList = this.pollFormObject.Tags.split(",");

      let PostBody = {
        title: this.pollFormObject.Title,
        description: this.pollFormObject.Description,
        tags: TagsList,
        category: this.pollFormObject.Category,
      };

      this._baseService.isLoader = true;

      let requestURL = this._Config.API_Root + this._Config.CreatePoll;
      let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
      this._baseService.doPOST(requestURL, PostBody, headerOptions)
        .subscribe(
          (response: any) => {

            let responseData: any = response;
            if (responseData.statusCode == 200) {
              this.uploadPollImage(responseData.data.id);

            }
            else {
              this._utilityService.showError('Error!', responseData.message);
              this._baseService.isLoader = false;

            }

          },
          (error: any) => {
            console.log("Rrror", error);
          }
        );

    }


  }



  /**
   * To Initialize and Upload Picture
   */
  uploadPollImage(PollID: any) {

    let requestURL = this._Config.API_Root + this._Config.UpdatePollPictureById;
    requestURL = requestURL.replace("{PollID}", PollID);

    /********** File Upload Configurations **********/
    this.apiURL = requestURL;
    // this.uploader = new FileUploader({ method: "PUT", url: this.apiURL, itemAlias: 'picture', authToken: 'Bearer ' + localStorage.getItem('userToken') });

    // Update uploader URL
    this.uploader.setOptions({ method: "PUT", url: this.apiURL, itemAlias: 'picture', authToken: 'Bearer ' + localStorage.getItem('userToken') });

    // Clear the item queue (somehow they will upload to the old URL)
    // this.uploader.clearQueue();

    this.uploader.uploadAll();
  }



  /**
   * cancel button event and Emit the Boolean Value True
   * @param event 
   */
  cancelCreatePoll() {
    this.isBack.emit(true);
  }


  onFileChange(event) {
    let self = this;
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      var file: File = event.target.files[0];
      var myReader: FileReader = new FileReader();

      myReader.onloadend = (e) => {
        self.tempPreview = myReader.result;
      }
      myReader.readAsDataURL(file);


    }
  }



  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  confirm(): void {
    // this.votePoll();
    this.cancelCreatePoll();
    this._utilityService.showSuccess('Success!', 'Poll Created successfully.');

    // this.message = 'Confirmed!';
    this.modalRef.hide();
  }

  decline(): void {
    // this.cancelCreatePoll();
    // this._utilityService.showSuccess('Success!', 'Poll Created successfully.');

    // this.message = 'Declined!';
    this.modalRef.hide();
  }


  votePoll() {

    if (this.pollFormObject.PredictionType == 'Month') {
      if (!this.voteMonth.month) {
        this._utilityService.showWarning("", "Prediction Month Could not be Empty!");
      }
      else {
        this.PostPollPrediction();
      }
    }
    else if (this.pollFormObject.PredictionType == 'Month/Year') {
      if (this.yearSelect == 'YYYY' || this.monthSelect == 'MM') {
        this._utilityService.showWarning("", "Prediction Month / Year Could not be Empty!");
      }
      else {

        let SelectedItem = {
          month: this.monthSelect,
          year: this.yearSelect
        };
        this.voteMonth = SelectedItem;

        this.PostPollPrediction();
      }

    }
    else if (this.pollFormObject.PredictionType == 'Year') {
      if (this.yearSelect == 'YYYY') {
        this._utilityService.showWarning("", "Prediction Year Could not be Empty!");
      }
      else {

        let SelectedItem = {
          month: '01',
          year: this.yearSelect
        };
        this.voteMonth = SelectedItem;

        this.PostPollPrediction();
      }
    }





  }


  PostPollPrediction() {
    if (this._baseService.currentUser.isLogedIn) {

      let PostBody = {
        month: this.voteMonth.month,
        year: this.voteMonth.year
      };

      this._baseService.isLoader = true;

      let requestURL = this._Config.API_Root + this._Config.PredictPollById;
      requestURL = requestURL.replace("{PollID}", this.createdPoll.id);

      let headerOptions = this._baseService.commonHeaders("withBearerUserToken");
      this._baseService.doPUT(requestURL, PostBody, headerOptions)
        .subscribe(
          (response: any) => {

            let responseData: any = response;
            if (responseData.statusCode == 200) {

              this._baseService.getActivePolls();
              this.cancelCreatePoll();
              this._utilityService.showSuccess('Success!', 'Poll Created successfully.');

            }
            else {
              this._utilityService.showError('Error!', responseData.message);
              this._baseService.isLoader = false;

            }

          },
          (error: any) => {
            console.log("Rrror", error);
          }
        );

    }
    else {
      this._utilityService.showWarning("", "Please Login and post your comment!");
    }
  }
}
