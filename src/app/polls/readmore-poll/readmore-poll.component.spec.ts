import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadmorePollComponent } from './readmore-poll.component';

describe('ReadmorePollComponent', () => {
  let component: ReadmorePollComponent;
  let fixture: ComponentFixture<ReadmorePollComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadmorePollComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadmorePollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
