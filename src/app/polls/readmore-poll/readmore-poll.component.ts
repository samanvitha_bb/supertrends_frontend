import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Routes, RouterModule, Router, ActivatedRoute } from "@angular/router";


/**** Config File ***/
import { APIConfig } from '../../apiconfig';

/**** Base Service File ***/
import { BaseService } from '../../Services/base.service';


@Component({
  selector: 'app-readmore-poll',
  templateUrl: './readmore-poll.component.html',
  styleUrls: ['./readmore-poll.component.scss']
})
export class ReadmorePollComponent implements OnInit {

  isEdit: boolean = false;
  showDetails: boolean = false;
  showReadMoreItems: boolean = false;
  selectedItem: any = {};
  selectedCategory: any = {id:"",name:"All"};

  SearchKeyword: any = "";
  isOrderby: boolean = true;

  ReadMoreType: any = "";
  selectedFilterTitle: any = "";


  defaultImage = 'assets/img/touch.jpg';
  offset = 0;
  
  constructor(public _Config: APIConfig, private _baseService: BaseService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {

    this._baseService.getActivePolls();

    this.ReadMoreType = localStorage.getItem("readMore_Poll");
    if (this.ReadMoreType) {


      //set Filter for selected Items here

      if (this.ReadMoreType == "Trending") {
        this.selectedFilterTitle = "Trending";
      }
      else if (this.ReadMoreType == "Newest") {
        this.selectedFilterTitle = "Newest";

      }
      else if (this.ReadMoreType == "Popular-Technology") {
        this.selectedFilterTitle = "Popular in Technology";

      }
      else if (this.ReadMoreType == "MyPolls") {
        this.selectedFilterTitle = "My Polls";
        this._baseService.getMyPolls();

      }


      this._baseService.getActivePolls();
    }
    else
      this.router.navigate(['/polls']);


  }


  /**
  * View Selected Poll Details
  * @param selectedItem 
  */
  ViewItem(selectedItem: any) {
    this.showDetails = true;
    this.selectedItem = selectedItem;
    window.scroll(0, 0);

  }


  /**
   * Poll Details page back button event
   * @param event 
   */
  handleBackChange(event: any) {
    this.showDetails = !event;
    this.ReadMoreType = !event;
    // this._baseService.getActivePolls();

  }


  /**
   * To make Order by Ascending or Descending
   * @param type 
   */
  selectOrderBy(type: boolean) {
    this.isOrderby = type;
  }


    /*********  Category Change event  ***********/
    selectCategory(selectedCategory: any) {
      if (selectedCategory == "All") {
        this.selectedCategory = { id: "", name: "All" };
      }
      else{
        this.selectedCategory = selectedCategory;
      }
    }

    
}

