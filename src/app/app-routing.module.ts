import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LaunchComponent } from './launch/launch.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { DiscussionComponent } from './discussion/discussion.component';
import { QuestionsComponent } from './questions/questions.component';
import { AmaComponent } from './ama/ama.component';
import { AmaDetailComponent } from './ama-detail/ama-detail.component';
import { ProfileComponent } from './profile/profile.component';
import { VoteComponent } from './vote/vote.component';
import { PollsComponent } from './polls/polls.component';
import { ReadmorePollComponent } from './polls/readmore-poll/readmore-poll.component';
import { PostsComponent } from './posts/posts.component';
import { ReadmorePostComponent } from './posts/readmore-post/readmore-post.component';
import { SocialAuthComponent } from './social-auth/social-auth.component';
import { NotificationComponent } from './notification/notification.component';

import { AdminPortalComponent } from './admin-portal/admin-portal.component';
import { AdminDashboardComponent } from './admin-portal/admin-dashboard/admin-dashboard.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {path: 'auth' , component: SocialAuthComponent},
  {path: 'launch' , component: LaunchComponent},
  {path: 'dashboard' , component: DashboardComponent},
  {path: 'home' , component: HomeComponent},
  {path: 'timeline' , component: HomeComponent},
  {path: 'discussion' , component: DiscussionComponent},
  {path: 'posts' , component: PostsComponent},
  {path: 'posts/readmore' , component: ReadmorePostComponent},
  {path: 'questions' , component: QuestionsComponent},
  {path: 'ama' , component: AmaComponent},
  {path: 'ama/detail/:id' , component: AmaDetailComponent},
  {path: 'profile' , component: ProfileComponent},
  {path: 'polls' , component: PollsComponent},
  {path: 'polls/readmore' , component: ReadmorePollComponent},
  {path: 'notifcations' , component: NotificationComponent},
  {path: 'admin-portal' , component: AdminPortalComponent},
  {path: 'admin/dashboard' , component: AdminDashboardComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
